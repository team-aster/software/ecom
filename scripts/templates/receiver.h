/** Functionality to parse incoming messages. */
%AUTO_GENERATED_NOTICE%

#pragma once

#include <stddef.h>
#include "%INCLUDE_PREFIX%/units.h"
#include "%INCLUDE_PREFIX%/sharedTypes.h"
#include "generated/bufferAccessor.h"

#ifdef __cplusplus
extern "C" {
#endif

/** An error during parsing of a message. */
typedef enum {
    /** The message type was not recognized. */
    INVALID_MESSAGE,

    /** The checksum of the message was invalid. */
    CHECKSUM_ERROR,

    /** A dynamic size was too large. */
    DYNAMIC_SIZE_TOO_LARGE,
} ParserError;


/** A handler of parsed messages. */
typedef struct MessageHandler {
    %MESSAGE_HANDLERS%

    /**
     * Handle an error during parsing of a message.
     *
     * @param handler The message handler.
     * @param error The error that occurred.
     */
    void (*errorHandler)(struct MessageHandler* handler, ParserError error);

    /** Private data of the message handler. */
    void* data;
} MessageHandler;


/**
 * Parse and handle a received message.
 *
 * @param buffer An accessor to a buffer to parse a message from.
 * @param handler An handler for parsed messages.
 * @return Whether the message was parsed and handled.
 */
extern bool parseMessage(BufferAccessor* buffer, MessageHandler* handler);

#ifdef __cplusplus
}
#endif
