/** Data types that are transmitted via telecommands and telemetry. */
%AUTO_GENERATED_NOTICE%

#pragma once

#include <stdint.h>
#include "generated/util.h"
#include "%INCLUDE_PREFIX%/units.h"

#ifdef __cplusplus
extern "C" {
#endif

%SHARED_CONSTANTS%
%SHARED_DATATYPES%

#ifdef __cplusplus
}
#endif
