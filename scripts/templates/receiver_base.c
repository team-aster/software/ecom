%AUTO_GENERATED_NOTICE%

#include "%INCLUDE_PREFIX%/receiver.h"
#include "generated/checksum.h"


bool parseMessage(BufferAccessor* buffer, MessageHandler* handler) {
    for (size_t availableSize = buffer->availableBytes(buffer);
         availableSize >= sizeof(TelemetryMessageHeader);
         buffer->advance(buffer, 1), availableSize--) {
        uint8_t byteRead;
        if (!buffer->peek(buffer, &byteRead, sizeof(byteRead), 0) || byteRead != SYNC_BYTE_1 ||
            !buffer->peek(buffer, &byteRead, sizeof(byteRead), 1) || byteRead != SYNC_BYTE_2) {
            continue;
        }
        TelemetryMessageHeader header;
        if (!buffer->peek(buffer, &header, sizeof(header), 0)) {
            return false;
        }
        switch (header.type) {
        %MESSAGE_HANDLERS%
        default:
            handler->errorHandler(handler, INVALID_MESSAGE);
        }
    }
    return false;
}
