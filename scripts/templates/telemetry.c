%AUTO_GENERATED_NOTICE%

#include <string.h>
#include <stddef.h>
#include "%INCLUDE_PREFIX%/telemetry.h"
#include "generated/checksum.h"


/**
 * Send the serialized telemetry.
 *
 * @param data The serialized telemetry in a buffer.
 * @param size The size of the serialized telemetry in bytes.
 * @return Whether writing the telemetry was successful.
 */
extern bool writeTelemetry(void* data, size_t size);
%TELEMETRY_STRUCTS%
%TELEMETRY_SENDERS%
