%AUTO_GENERATED_NOTICE%

#include <inttypes.h>
#include <memory.h>
#include "%INCLUDE_PREFIX%/telemetry.h"
#include "%INCLUDE_PREFIX%/receiver.h"
#include "%INCLUDE_PREFIX%/checksum.h"

/**
 * @note This function is only called when a debug command is received.
 * @note This function has to be provided by the ECom user.
 * @return Whether executing debug commands is currently enabled.
 */
extern bool areDebugCommandsEnabled(void);

/**
 * Print the formatted debug message.
 *
 * @note This function has to be provided by the ECom user.
 * @param format The format string.
 * @param ... Format values.
 */
extern void printDebug(const char* format, ...);
%CONFIG_HANDLERS%

bool parseMessage(BufferAccessor* buffer, MessageHandler* handler) {
    for (size_t availableSize = buffer->availableBytes(buffer);
         availableSize >= sizeof(TelecommandMessageHeader);
         buffer->advance(buffer, 1), availableSize--) {
        uint8_t byteRead;
        if (!buffer->peek(buffer, &byteRead, sizeof(byteRead), 0) || byteRead != SYNC_BYTE_1 ||
            !buffer->peek(buffer, &byteRead, sizeof(byteRead), 1) || byteRead != SYNC_BYTE_2) {
            buffer->advance(buffer, 1);
            availableSize--;
            continue;
        }
        TelecommandMessageHeader header;
        if (!buffer->peek(buffer, &header, sizeof(header), 0)) {
            return false;
        }
        switch (header.type) {
        %MESSAGE_HANDLERS%
        default:
            handler->errorHandler(handler, INVALID_MESSAGE);
        }
    }
    return false;
}
