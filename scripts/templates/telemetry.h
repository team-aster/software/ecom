/** Functionality for sending telemetry. */
%AUTO_GENERATED_NOTICE%

#pragma once

#include "%INCLUDE_PREFIX%/units.h"
#include "%INCLUDE_PREFIX%/sharedTypes.h"

#ifdef __cplusplus
extern "C" {
#endif

%TELEMETRY_HANDLERS%

#ifdef __cplusplus
}
#endif
