#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import sys

from enum import Enum
from typing import Any, List
from numbers import Number
from difflib import SequenceMatcher
from textwrap import dedent
from argparse import ArgumentParser
from collections import OrderedDict

try:
    from ecom.message import DependantTelecommandDatapointType
except ImportError:
    # If ecom is not installed, try finding it up the directory tree from the current file
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

    from ecom.message import DependantTelecommandDatapointType

from ecom.database import CommunicationDatabase, CommunicationDatabaseError, Unit, \
    UnknownTypeError, ConfigurationValueDatapoint, ConfigurationValueResponseType
from ecom.datatypes import TypeInfo, StructType, ArrayType, DynamicSizeError


class CCodeGenerator:
    """ A code generator that generates C code from the database. """
    _MAX_LINE_LENGTH = 100
    _SNAKE_CASE_PATTERN = re.compile(r'(?<!^)(?=[A-Z])')
    _BASE_TYPE_PRINT_FORMATS = {
        TypeInfo.BaseType.INT8: '%" PRId8 "',
        TypeInfo.BaseType.UINT8: '%" PRIu8 "',
        TypeInfo.BaseType.BOOL: '%" PRIu8 "',
        TypeInfo.BaseType.INT16: '%" PRId16 "',
        TypeInfo.BaseType.UINT16: '%" PRIu16 "',
        TypeInfo.BaseType.INT32: '%" PRId32 "',
        TypeInfo.BaseType.UINT32: '%" PRIu32 "',
        TypeInfo.BaseType.INT64: '%" PRId64 "',
        TypeInfo.BaseType.UINT64: '%" PRIu64 "',
        TypeInfo.BaseType.FLOAT: '%f',
        TypeInfo.BaseType.DOUBLE: '%f',
        TypeInfo.BaseType.CHAR: '%c',
        TypeInfo.BaseType.BYTES: '%c',
    }
    _BASE_TYPE_C_TYPES = {
        TypeInfo.BaseType.INT8: 'int8_t',
        TypeInfo.BaseType.UINT8: 'uint8_t',
        TypeInfo.BaseType.BOOL: 'bool',
        TypeInfo.BaseType.INT16: 'int16_t',
        TypeInfo.BaseType.UINT16: 'uint16_t',
        TypeInfo.BaseType.INT32: 'int32_t',
        TypeInfo.BaseType.UINT32: 'uint32_t',
        TypeInfo.BaseType.INT64: 'int64_t',
        TypeInfo.BaseType.UINT64: 'uint64_t',
        TypeInfo.BaseType.FLOAT: 'float',
        TypeInfo.BaseType.DOUBLE: 'double',
        TypeInfo.BaseType.CHAR: 'char',
        TypeInfo.BaseType.BYTES: 'uint8_t',
    }

    def __init__(self, database: CommunicationDatabase, prefix=None):
        """
        Create a new code generator.

        :param database: The communication database.
        :param prefix: A prefix that will be applied to the generated code.
        """
        super().__init__()
        self._database = database
        self._prefix = prefix
        codegenTemplateDirectories = [
            os.path.join(sys.prefix, 'ecom', 'templates'),
            os.path.join(os.path.abspath(os.path.dirname(__file__)), 'templates'),
        ]
        for path in codegenTemplateDirectories:
            try:
                if os.path.isdir(path):
                    self._templateDir = path
                    break
            except IOError:
                pass
        else:
            raise RuntimeError('Unable to locate code generation template directory in ony of the following locations: '
                               + '\n'.join(codegenTemplateDirectories))

    def generate(self, sourceDir, headerDir):
        """
        Generate code in the given directory based on the database.

        :param sourceDir: The path to the directory to generate the source code into.
        :param headerDir: The path to the directory to generate the headers into.
        """
        generatedSharedSourceDir = os.path.join(sourceDir, 'generated')
        generatedSharedIncludeDir = os.path.join(headerDir, 'generated')
        if self._prefix is not None:
            generatedSourceDir = os.path.join(generatedSharedSourceDir, self._prefix)
            generatedIncludeDir = os.path.join(generatedSharedIncludeDir, self._prefix)
        else:
            generatedSourceDir = generatedSharedSourceDir
            generatedIncludeDir = generatedSharedIncludeDir
        os.makedirs(generatedIncludeDir, exist_ok=True)
        os.makedirs(generatedSourceDir, exist_ok=True)
        self._generateSharedCode(generatedSharedSourceDir, generatedSharedIncludeDir)
        self._generateUnitsSource(generatedIncludeDir)
        knownTypes = self._generateSharedTypesHeader(generatedIncludeDir)
        messageHandlers = self._generateReceiverHeader(generatedIncludeDir)
        self._generateReceiverSource(generatedSourceDir, messageHandlers)
        telemetryHandlers = self._generateTelemetryHeader(generatedIncludeDir)
        self._generateTelemetrySource(generatedSourceDir, telemetryHandlers, knownTypes)

    def _generateSharedCode(self, generatedSourceDir, generatedIncludeDir):
        """
        Generate a header and source file are possible shared
        with other communication database codegen.

        :param generatedSourceDir: The path to the source-directory for generated code.
        :param generatedIncludeDir: The path to the include-directory for generated code.
        """
        self._writeFormattedTemplate(os.path.join(generatedIncludeDir, 'util.h'), 'util.h')
        self._writeFormattedTemplate(
            os.path.join(generatedIncludeDir, 'bufferAccessor.h'), 'bufferAccessor.h')
        self._writeFormattedTemplate(
            os.path.join(generatedSourceDir, 'bufferAccessor.c'), 'bufferAccessor.c')
        self._writeFormattedTemplate(os.path.join(generatedIncludeDir, 'checksum.h'), 'checksum.h')
        self._writeFormattedTemplate(os.path.join(generatedSourceDir, 'checksum.c'), 'checksum.c')

    def _generateSharedTypesHeader(self, generatedIncludeDir):
        """
        Generate a header file that defines all shared types and constants.

        :param generatedIncludeDir: The path to the 'include' directory for generated content.
        :return: Types that are now known to the C code.
        """
        knownTypes = []
        self._writeFormattedTemplate(
            os.path.join(generatedIncludeDir, 'sharedTypes.h'), 'sharedTypes.h',
            shared_constants=self._writeSharedConstants,
            shared_datatypes=lambda file: self._writeSharedDataTypes(file, knownTypes),
        )
        return knownTypes

    def _writeSharedConstants(self, outputFile):
        """
        Write the code to define all shared constants.

        :param outputFile: The file to write the shared constants to.
        """
        for name, constant in self._database.constants.items():
            outputFile.write(self._formatDocumentation(constant.description))
            valueString = self._formatValue(constant.value, constant.type)
            outputFile.write(self._formatLines(
                f'#define {name} {valueString}', ' ' * 4,
                maxLineLength=self._MAX_LINE_LENGTH - 2).replace('\n', ' \\\n') + '\n')

    def _writeSharedDataTypes(self, outputFile, knownTypes):
        """
        Write the code to define all known shared datatypes into the file.

        :param outputFile: The file to write the shared datatype definitions to.
        :param knownTypes: A list that will be filled with all names of datatypes that were written.
        """
        for name, typInfo in self._database.dataTypes.items():
            outputFile.write('\n')
            self._writeType(outputFile, name, typInfo, knownTypes)
            knownTypes.append(name)

    def _generateReceiverHeader(self, generatedIncludeDir):
        """
        Generate a header that defines a handler type for received messages.

        :param generatedIncludeDir: The path to the 'include' directory for generated content.
        :return: Information about the message handlers.
        """
        messageHandlers = OrderedDict()
        if self._database.telecommandTypes:
            self._writeFormattedTemplate(
                os.path.join(generatedIncludeDir, 'receiver.h'), 'receiver.h',
                message_handlers=lambda file: self._writeTelecommandHandlerDefinitions(file, messageHandlers)
            )
        return messageHandlers

    def _writeTelecommandHandlerDefinitions(self, outputFile, telecommandHandlers):
        """
        Write the code to define prototypes for all telecommand handler functions to the file.

        :param outputFile: The file to write the telecommand handler functions prototypes to.
        :param telecommandHandlers: An OrderedDict that will be filled with information
                                    about the telecommand handlers.
        """
        isFirstLine = True
        for telecommand in self._database.telecommandTypes:
            handlerFunction = 'handleCommand' + self._snakeToCamelCase(telecommand.id.name)
            isConfigHandler = False
            handlerArguments = OrderedDict()
            for argument in telecommand.data:
                if not isinstance(argument, ConfigurationValueDatapoint):
                    typeName = self._formatArgumentType(argument.type)
                else:
                    typeName = 'const uint8_t*'
                    isConfigHandler = True
                handlerArguments[self._toCamelCase(argument.name)] = typeName, argument
            returnType = 'void'
            if telecommand.response is not None:
                if isinstance(telecommand.response, ConfigurationValueResponseType):
                    isConfigHandler = True
                    handlerArguments['responseBuffer'] = 'uint8_t*', None
                else:
                    returnType = self._getTypeName(telecommand.response.typeInfo)
                    if issubclass(telecommand.response.typeInfo.type, StructType):
                        returnType = f'const {returnType}*'
            telecommandHandlers[handlerFunction] = (telecommand, handlerArguments, returnType)
            if isConfigHandler:
                continue
            if isFirstLine:
                isFirstLine = False
            else:
                outputFile.write('\n\n    ')
            outputFile.write(self._formatDocumentation(
                self._generateFunctionDocumentation(
                    f'Handle the {telecommand.id.name} telecommand.', OrderedDict([
                        ('handler', 'The message handler.'),
                        *((self._toCamelCase(argument.name), argument.description) for argument in telecommand.data)
                    ]), None if telecommand.response is None else telecommand.response.description), indent=1)[4:])
            handlerArgumentsStr = ', '.join(('struct MessageHandler* handler',
                                             *(f'{typ} {name}' for name, (typ, _) in handlerArguments.items())))
            outputFile.write(self._formatLines(f'    {returnType} (*{handlerFunction})({handlerArgumentsStr});',
                                               continueIndent='        '))

    def _generateReceiverSource(self, generatedSourceDir, messageHandlers):
        """
        Generate the source file for a function that can parse incoming messages and dispatch them to a handler.

        :param generatedSourceDir: The path to the source directory for generated content.
        :param messageHandlers: Information about the message handler functions.
        """
        if messageHandlers:
            self._writeFormattedTemplate(
                os.path.join(generatedSourceDir, 'receiver.c'), 'receiver.c',
                config_handlers=lambda file: self._writeConfigHandlers(file, messageHandlers),
                message_handlers=lambda file: self._writeTelecommandHandlers(file, messageHandlers),
            )

    def _writeConfigHandlers(self, outputFile, telecommandHandlers):
        """
        Write the code for the config getter and setter helper functions to the file,
        if there are any config telecommands defined.

        :param outputFile: The file to write the config helper functions prototypes to.
        :param telecommandHandlers: An OrderedDict with information about the telecommand handlers.
        """
        haveSetter = any(isinstance(argument, ConfigurationValueDatapoint) for _, arguments, _ in
                         telecommandHandlers.values() for _, argument in arguments.values())
        haveGetter = any(isinstance(telecommand.response, ConfigurationValueResponseType)
                         for telecommand, _, _ in telecommandHandlers.values())
        if haveSetter or haveGetter:
            self._writeConfigHelper(outputFile)
        if haveSetter:
            self._writeConfigSetterTelecommandHandler(outputFile)
        if haveGetter:
            self._writeConfigGetterTelecommandHandler(outputFile)

    def _writeTelecommandHandlers(self, outputFile, telecommandHandlers):
        """
        Write the code to call the handlers for each telecommand to the file.

        :param outputFile: The file to write the telecommand handler dispatch code to.
        :param telecommandHandlers: An OrderedDict with information about the telecommand handlers.
        """
        isFirstLine = True
        for handlerFunction, (
                telecommand, handlerArguments, returnType) in telecommandHandlers.items():
            if isFirstLine:
                isFirstLine = False
            else:
                outputFile.write('\n        ')
            outputFile.write(f'case COMMAND_{telecommand.id.name}: {{\n')
            self._writeMessageArgumentsParser(outputFile, telecommand, handlerArguments)
            printArguments = f'"Received {telecommand.id.name}'
            argumentsFormats = []
            isConfigCommand = False
            for argumentName, (argumentType, argument) in handlerArguments.items():
                if argument is None or isinstance(argument, ConfigurationValueDatapoint):
                    isConfigCommand = True
                    continue
                if not issubclass(argument.type.type, StructType):
                    printFormat = self._getPrintFormat(argument.type)
                    accessor = argumentName
                    if issubclass(argument.type.type, Enum):
                        baseType = self._getBaseType(argument.type)
                        accessor = f'({baseType}) {argumentName}'
                    if isinstance(argument.type, Unit):
                        accessor += '.' + argument.type.name
                    argumentsFormats.append((argumentName, printFormat, accessor))
            if argumentsFormats:
                printArguments += '(' + ', '.join(
                    f'{argumentName}={argumentsFormat}'
                    for argumentName, argumentsFormat, _ in argumentsFormats
                ) + ')", '
                printArguments += ', '.join(accessor for _, _, accessor in argumentsFormats)
            else:
                printArguments += '"'
            outputFile.write(self._formatLines(f'            printDebug({printArguments});\n',
                                               continueIndent='                '))
            if telecommand.isDebug:
                outputFile.write(
                    '            if (!areDebugCommandsEnabled()) {\n'
                    '                return true;\n'
                    '            }\n')
            if isConfigCommand:
                argumentsStr = ', '.join(handlerArguments)
            else:
                argumentsStr = ', '.join(('handler', *handlerArguments))
                handlerFunction = f'handler->{handlerFunction}'
            if telecommand.response is None:
                outputFile.write(f'            {handlerFunction}({argumentsStr});\n')
                outputFile.write(f'            sendTelemetryAcknowledge(header.counter);\n')
            else:
                if isinstance(telecommand.response, ConfigurationValueResponseType):
                    outputFile.write(
                        f'            uint8_t responseBuffer[MAX_TELECOMMAND_RESPONSE_SIZE] = {{0}};\n')
                    outputFile.write(f'            {handlerFunction}({argumentsStr});\n')
                else:
                    responseName = self._toCamelCase(telecommand.response.name)
                    responseType = self._getTypeName(telecommand.response.typeInfo)
                    responseIsStruct = issubclass(telecommand.response.typeInfo.type, StructType)
                    if responseIsStruct:
                        responseType = f'const {responseType}*'
                    outputFile.write(f'            {responseType} {responseName} '
                                     f'= {handlerFunction}({argumentsStr});\n')
                    outputFile.write(
                        f'            uint8_t responseBuffer[MAX_TELECOMMAND_RESPONSE_SIZE] = {{0}};\n')
                    if responseIsStruct:
                        # noinspection SpellCheckingInspection
                        outputFile.write(f'            memcpy(responseBuffer, {responseName},'
                                         f' sizeof(*{responseName}));\n')
                    else:
                        outputFile.write(f'            (({responseType}*) responseBuffer)[0] = {responseName};\n')
                outputFile.write(f'            sendTelemetryResponse(header.counter, responseBuffer);\n')
            outputFile.write('            return true;\n')
            outputFile.write('        }')

    def _generateTelemetryHeader(self, generatedIncludeDir):
        """
        Generate a header that defines functions for sending each telemetry message.

        :param generatedIncludeDir: The path to the 'include' directory for generated content.
        :return: Information about the telemetry sender functions.
        """
        telemetryHandlers = OrderedDict()
        self._writeFormattedTemplate(
            os.path.join(generatedIncludeDir, 'telemetry.h'), 'telemetry.h',
            telemetry_handlers=lambda file: self._writeTelemetryHandlerDefinitions(
                file, telemetryHandlers),
        )
        return telemetryHandlers

    def _writeTelemetryHandlerDefinitions(self, outputFile, telemetryHandlers):
        """
        Write the code to define prototypes for all telemetry handler functions to the file.

        :param outputFile: The file to write the telemetry handler functions prototypes to.
        :param telemetryHandlers: An OrderedDict that will be filled with information
                                  about the telemetry handlers.
        """
        for telemetryType in self._database.telemetryTypes:
            outputFile.write('\n' + self._formatDocumentation(
                self._generateFunctionDocumentation(
                    f'Send a {telemetryType.id.name} message to the ground station.',
                    OrderedDict([(self._toCamelCase(datapoint.name), datapoint.description)
                                 for datapoint in telemetryType.data]),
                    'Whether or not the telemetry was send successfully.')))
            handlerFunction = 'sendTelemetry' + self._snakeToCamelCase(telemetryType.id.name)
            arguments = OrderedDict()
            for datapoint in telemetryType.data:
                sizeName = None
                if issubclass(datapoint.type.type, ArrayType):
                    try:
                        len(datapoint.type.type)
                    except DynamicSizeError as error:
                        sizeName = self._toCamelCase(error.sizeMember)
                arguments[self._toCamelCase(datapoint.name)] = \
                    self._formatArgumentType(datapoint.type), sizeName
            handlerArgumentsStr = ', '.join(typ[0] + ' ' + name for name, typ in arguments.items())
            telemetryHandlers[handlerFunction] = (telemetryType, arguments, handlerArgumentsStr)
            if not handlerArgumentsStr:
                handlerArgumentsStr = 'void'
            handlerArgumentsStr = self._formatLines(
                handlerArgumentsStr, '    ', len(f'extern bool {handlerFunction}('))
            outputFile.write(f'extern bool {handlerFunction}({handlerArgumentsStr});\n')

    def _generateTelemetrySource(self, generatedSourceDir, telemetryHandlers, knownTypes):
        """
        Generate a source file that contains functions for sending each telemetry message.

        :param generatedSourceDir: The path to the source directory for generated content.
        :param telemetryHandlers: Information about the telemetry sender functions.
        :param knownTypes: Data types that are already known to the code.
        """
        if not telemetryHandlers:
            return
        self._writeFormattedTemplate(
            os.path.join(generatedSourceDir, 'telemetry.c'), 'telemetry.c',
            telemetry_structs=lambda file: self._writeTelemetryDataStructs(file, knownTypes),
            telemetry_senders=lambda file: self._writeTelemetrySenders(file, telemetryHandlers),
        )

    def _writeTelemetryDataStructs(self, outputFile, knownTypes):
        """
        Write the code to define types for all telemetry data to the file.

        :param outputFile: The file to write the telemetry data types to.
        :param knownTypes: Data types that are already known to the code.
        """
        headerTypeInfo = self._database.getTypeInfo('TelemetryMessageHeader')
        for telemetryType in self._database.telemetryTypes:
            name = 'Telemetry' + self._snakeToCamelCase(telemetryType.id.name) + 'Data'
            outputFile.write('\n')
            description = f'Data for the {telemetryType.id.name} message.'
            typ = StructType(name, OrderedDict([
                (datapoint.name, TypeInfo(datapoint.type.type, datapoint.name, self._getTypeName(
                    datapoint.type), datapoint.description)) for datapoint in telemetryType.data
            ]), description)
            dataTypInfo = TypeInfo(typ, name, None, description)
            self._writeType(outputFile, name, dataTypInfo, knownTypes)
            knownTypes.append(name)
            name = 'Telemetry' + self._snakeToCamelCase(telemetryType.id.name) + 'Type'
            description = f'A {telemetryType.id.name} message.'
            typ = StructType(name, OrderedDict([
                ('header', TypeInfo(
                    headerTypeInfo.type, 'header', headerTypeInfo.name, 'The message header.')),
                ('data', TypeInfo(
                    dataTypInfo.type, 'data', dataTypInfo.name, 'The message data.'))
            ]), description)
            typInfo = TypeInfo(typ, name, None, description)
            outputFile.write('\n')
            self._writeType(outputFile, name, typInfo, knownTypes)
            knownTypes.append(name)

    def _writeTelemetrySenders(self, outputFile, telemetryHandlers):
        """
        Write the code which defines functions to send each telemetry to the file.

        :param outputFile: The file to write the telemetry sender functions to.
        :param telemetryHandlers: Information about the telemetry sender functions.
        """
        headerTypeInfo = self._database.getTypeInfo('TelemetryMessageHeader')
        for handlerFunction, (
                telemetryType, handlerArguments, handlerArgumentsStr) in telemetryHandlers.items():
            dataTypeName = 'Telemetry' + self._snakeToCamelCase(telemetryType.id.name) + 'Type'
            handlerArgumentsStr = self._formatLines(
                handlerArgumentsStr, '    ', len(f'bool {handlerFunction}('))
            outputFile.write(f'\n\nbool {handlerFunction}({handlerArgumentsStr}) ')
            dynamicSizes = []
            for listName, (_, sizeName) in handlerArguments.items():
                if len(dynamicSizes) != 0:
                    raise RuntimeError('Variable sized members must be at the end of the telemetry list currently')
                if sizeName is not None:
                    if len(dynamicSizes) != 0:
                        raise RuntimeError('Multiple variable sized members are currently not supported')
                    dynamicSizes.append(f'{sizeName} * sizeof({listName}[0])')
            outputFile.write('{\n' + self._formatLines(
                f'    size_t messageSize = sizeof({dataTypeName})' +
                ''.join(' + ' + size for size in dynamicSizes) + ';', ' ' * 8) + '\n')
            if dynamicSizes:
                outputFile.write(dedent(f'''\
                    uint8_t dataBuffer[messageSize];
                    {dataTypeName}* data = ({dataTypeName}*) dataBuffer;
                |''')[:-1])
                accessor = '->'
            else:
                outputFile.write(f'    {dataTypeName} data;\n')
                accessor = '.'
            for childName, childTypeInfo in headerTypeInfo.type:
                if childName == 'checksum':
                    continue
                if childName == 'type':
                    value = 'TELEMETRY_' + telemetryType.id.name
                else:
                    if childTypeInfo.default is None:
                        raise ValueError(
                            f'Telemetry header has member {childName} without a default value')
                    if childTypeInfo.default.constantName is None:
                        value = childTypeInfo.defaultValue
                    else:
                        value = childTypeInfo.default.constantName
                childName = self._toCamelCase(childName)
                cast = ''
                if issubclass(childTypeInfo.type, Enum):
                    cast = f'({self._getBaseType(childTypeInfo)}) '
                outputFile.write(f'    data{accessor}header.{childName} = {cast}{value};\n')
            for datapoint, (argument, (argumentTypeStr, sizeName)) in zip(
                    telemetryType.data, handlerArguments.items()):
                if argumentTypeStr.endswith('*'):
                    if sizeName is None:
                        sizeName = f'sizeof(data{accessor}data.{argument})'
                    else:
                        sizeName += f' * sizeof({argument}[0])'
                    # noinspection SpellCheckingInspection
                    outputFile.write(
                        f'    memcpy(&data{accessor}data.{argument}, {argument}, {sizeName});\n')
                else:
                    cast = ''
                    if issubclass(datapoint.type.type, Enum):
                        cast = f'({self._getBaseType(datapoint.type)}) '
                    outputFile.write(f'    data{accessor}data.{argument} = {cast}{argument};\n')
                self._generateSizeStaticAssertion(outputFile, datapoint.type, indent=1)
            outputFile.write(
                f'    data{accessor}header.checksum = calculateChecksum(\n'
                f'        &((uint8_t*) &data{accessor}header.checksum)'
                f'[sizeof(data{accessor}header.checksum)],\n'
                f'        messageSize - sizeof(data{accessor}header.syncByte1)\n'
                f'        - sizeof(data{accessor}header.syncByte2) - '
                f'sizeof(data{accessor}header.checksum));\n')
            if dynamicSizes:
                outputFile.write('    return writeTelemetry(data, messageSize);\n}')
            else:
                outputFile.write('    return writeTelemetry(&data, messageSize);\n}')

    def _generateUnitsSource(self, generatedIncludeDir):
        """
        Generate the source that defines unit types.

        :param generatedIncludeDir: The path to the 'include' directory for generated content.
        """
        if not self._database.units:
            return
        self._writeFormattedTemplate(
            os.path.join(generatedIncludeDir, 'units.h'), 'units.h', units=self._writeUnits,
            operator_implementations=lambda file: file.write('\n'.join(
                f'OPERATOR_IMPLEMENTATION({self._resolveUnitName(name)}, '
                f'{self._BASE_TYPE_C_TYPES[unitVariants[0].getBaseType(self._database)]})' for name, unitVariants
                in self._database.units.items() if issubclass(unitVariants[0].type, Number))))

    def _writeUnits(self, outputFile):
        """
        Write the code to define all known units into the file.

        :param outputFile: The file to write the unit definitions to.
        """
        for unitVariants in self._database.units.values():
            for i, unit in enumerate(unitVariants):
                if unit.description:
                    outputFile.write(self._formatDocumentation(
                        unit.description, forceMultiline=True))
                baseTypeName = self._BASE_TYPE_C_TYPES[unit.getBaseType(self._database)]
                if i == 0:
                    name = self._resolveUnitName(unit.name)
                    outputFile.write(f'UNIT_TYPE({baseTypeName}, {name});\n')
                else:
                    name = self._getUnitVariantName(unitVariants[0], unit)
                    variantName = name.replace(unit.name, '', 1)
                    outputFile.write(f'UNIT_VARIANT_TYPE({baseTypeName}, '
                                     f'{unit.name}, {variantName});\n')
                outputFile.write(f'#define {name}_t_CONSTRUCTOR(value) (({name}_t) {{value}})\n')
                outputFile.write(self._formatLines(
                    f'#define {name}_t(...) ADD_UNIT_CONSTRUCTOR({name}_t, '
                    f'{name}_t_CONSTRUCTOR, __VA_ARGS__)',
                    ' ' * 4, maxLineLength=self._MAX_LINE_LENGTH - 2).replace('\n', ' \\\n'))
                outputFile.write('\n\n')

    def _writeConfigSetterTelecommandHandler(self, telecommandHandlerFile):
        """
        Write the handler function for a configuration setter telecommand.

        :param telecommandHandlerFile: The open file where the telecommand
                                       handler should be written to.
        """
        telecommandHandlerFile.write(dedent("""\

        /**
         * Start an update to the configuration. To persist the changes, commitConfigUpdate must be called.
         * @see commitConfigUpdate
         * @note This function has to be provided by the ECom user.
         *
         * @return The live configuration object that can be changed or NULL on failure.
         */
        extern Configuration* beginConfigUpdate(void);
        
        /**
         * Finish and persist the changes to the configuration.
         * @see beginConfigUpdate
         * @note This function has to be provided by the ECom user.
         */
        extern void commitConfigUpdate(void);

        /**
         * Handle the telecommand to change a config item.
         *
         * @param config The config item to change.
         * @param value A pointer to the new value of the config item.
         */
        static void handleCommandSetConfig(ConfigurationId configItem, const uint8_t* value) {
            Configuration* config = beginConfigUpdate();
            if (!config) {
                printError("Failed to update configuration: Failed start config update");
                return;
            }
            switch (configItem) {
        """))
        for config in self._database.configurations:
            telecommandHandlerFile.write(f'    case {self._toScreamingSnakeCase(config.name)}:\n')
            # noinspection SpellCheckingInspection
            telecommandHandlerFile.write(self._formatLines(
                f'        memcpy(&config->{self._toCamelCase(config.name)}, value, '
                f'sizeof(config->{self._toCamelCase(config.name)}));', ' ' * 12) + '\n')
            telecommandHandlerFile.write('        break;\n')
        telecommandHandlerFile.write('    default:\n')
        telecommandHandlerFile.write('        printError("Got unknown config %u", (unsigned int) configItem);\n')
        telecommandHandlerFile.write('    }\n')
        telecommandHandlerFile.write('    commitConfigUpdate();\n')
        telecommandHandlerFile.write('}\n')

    def _writeConfigGetterTelecommandHandler(self, telecommandHandlerFile):
        """
        Write the handler function for a configuration getter telecommand.

        :param telecommandHandlerFile: The open file where the telecommand
                                       handler should be written to.
        """
        telecommandHandlerFile.write(dedent("""\

        /**
         * @note This function has to be provided by the ECom user.
         * @return The current configuration.
         */
        extern const Configuration* getConfig(void);

        /**
         * Handle the telecommand to read a config item.
         *
         * @param config The config item to read.
         */
        static void handleCommandGetConfig(ConfigurationId configItem, uint8_t* responseBuffer) {
            const Configuration* config = getConfig();
            switch (configItem) {
        """))
        for config in self._database.configurations:
            telecommandHandlerFile.write(f'    case {self._toScreamingSnakeCase(config.name)}:\n')
            configName = self._toCamelCase(config.name)
            # noinspection SpellCheckingInspection
            telecommandHandlerFile.write(self._formatLines(
                f'        memcpy(responseBuffer, (const uint8_t*) &config->{configName}, '
                f'sizeof(config->{configName}));', ' ' * 12) + '\n')
            telecommandHandlerFile.write('        break;\n')
        telecommandHandlerFile.write('    default:\n')
        telecommandHandlerFile.write('        printError("Got unknown config %u", (unsigned int) configItem);\n')
        telecommandHandlerFile.write('    }\n}')

    def _writeConfigHelper(self, telecommandHandlerFile):
        """
        Write the handler function for a configuration setter telecommand.

        :param telecommandHandlerFile: The open file where the telecommand
                                       handler should be written to.
        """
        telecommandHandlerFile.write('\n')
        telecommandHandlerFile.write(dedent("""\
        /**
         * Print the formatted error message.
         *
         * @note This function has to be provided by the ECom user.
         * @param format The format string.
         * @param ... Format values.
         */
        extern void printError(const char* format, ...);
        
        /**
         * @param config A configuration identifier.
         * @return The size of the corresponding configuration values type in bytes.
         */
        static size_t getConfigValueSize(ConfigurationId config) {
            switch (config) {
        """))
        for config in self._database.configurations:
            telecommandHandlerFile.write(f'    case {self._toScreamingSnakeCase(config.name)}:\n')
            telecommandHandlerFile.write(
                f'        return sizeof(((Configuration*) NULL)->{self._toCamelCase(config.name)});\n')
        telecommandHandlerFile.write('    default:\n')
        telecommandHandlerFile.write('        return 0;\n')
        telecommandHandlerFile.write('    }\n')
        telecommandHandlerFile.write('}')

    def _writeMessageArgumentsParser(self, outputFile, messageType, arguments, reservedNames=None, indent=3):
        """
        Write the code to parse message arguments from a buffer accessor to the file.
        The arguments will be available in the C code as variables.

        :param outputFile: The file to write the message arguments parser code to.
        :param messageType: The message type whose arguments should be parsed.
        :param arguments: An OrderedDict with information about the arguments.
        """
        indentString = '    ' * indent
        haveDynamicSize = False
        try:
            if any(isinstance(datapoint, DependantTelecommandDatapointType) for datapoint in messageType.data):
                raise DynamicSizeError('')
            requiredNumBytes = sum(dataPoint.type.getSize(self._database) for dataPoint in messageType.data)
            outputFile.write(indentString + f'size_t dataSize = {requiredNumBytes};\n')
            if requiredNumBytes:
                outputFile.write(indentString + 'if (buffer->availableBytes(buffer) < dataSize) {\n')
                outputFile.write(indentString + '    return false;\n')
                outputFile.write(indentString + '}\n')
        except DynamicSizeError:
            outputFile.write(indentString + 'size_t dataSize = 0;\n')
            haveDynamicSize = True
        offset = 0
        currentReservedNames = [
            'dataSize', 'buffer', 'handler', 'byteRead', 'availableSize', 'configSize', 'checksumStartOffset']
        if reservedNames is not None:
            currentReservedNames.extend(reservedNames)
        nameMapping = {}
        for argumentName, (argumentType, dataPoint) in arguments.items():
            if dataPoint is None:
                continue
            while argumentName in currentReservedNames:
                newName = argumentName + '_'
                nameMapping[argumentName] = newName
                argumentName = newName
                currentReservedNames.append(newName)
            if isinstance(dataPoint, ConfigurationValueDatapoint):
                outputFile.write(indentString + 'size_t configSize = getConfigValueSize((ConfigurationId) config);\n')
                outputFile.write(indentString + 'if (configSize == 0) {\n')
                outputFile.write(indentString + '    printError("Invalid config value: %u", (unsigned int) config);\n')
                outputFile.write(indentString + '    buffer->advance(buffer, 2);\n')
                outputFile.write(indentString + '    availableSize -= 2;\n')
                outputFile.write(indentString + '    continue;\n')
                outputFile.write(indentString + '}\n')
                dataPointType = self._database.parseKnownTypeInfo('uint8[.configSize]', 'ConfigValue')
                sizeMember = 'configSize'
            else:
                dataPointType = dataPoint.type
                sizeMember = None
                if haveDynamicSize and issubclass(dataPointType.type, ArrayType):
                    try:
                        len(dataPointType.type)
                    except DynamicSizeError as error:
                        sizeMember = nameMapping.get(error.sizeMember, error.sizeMember)
                        outputFile.write(indentString + f'if ({sizeMember} > 512) {{\n')
                        outputFile.write(indentString + '    handler->errorHandler(handler, DYNAMIC_SIZE_TOO_LARGE);\n')
                        outputFile.write(indentString + '    continue;\n')
                        outputFile.write(indentString + '}\n')
            declaration = self._formatVariableDeclaration(
                dataPointType, argumentName, useEnumBaseType=True)
            if sizeMember is not None:
                declaration = declaration.replace(f'.{sizeMember}', sizeMember)
            outputFile.write(indentString + f'{declaration};\n')
            if issubclass(dataPointType.type, Enum):
                argumentType = self._getBaseType(dataPointType)
            offsetString = ' + dataSize' if haveDynamicSize else \
                '' if offset == 0 else f' + {offset}'
            peekCall = f'buffer->peek(buffer, &{argumentName}, sizeof({argumentName}), ' \
                       f'sizeof(header){offsetString})'
            if not haveDynamicSize:
                outputFile.write(indentString + f'{peekCall};\n')
                offset += dataPointType.getSize(self._database)
            else:
                outputFile.write(indentString + f'if (!{peekCall}) {{\n')
                outputFile.write(indentString + '    return false;\n')
                outputFile.write(indentString + '}\n')
                size = f'sizeof({argumentType})' if sizeMember is None else sizeMember
                outputFile.write(indentString + f'dataSize += {size};\n')
            self._generateSizeStaticAssertion(outputFile, dataPointType, indent=3)
        outputFile.write(indentString + dedent('''\
        size_t checksumStartOffset =
            sizeof(header.syncByte1) + sizeof(header.syncByte2) + sizeof(header.checksum);
        if (header.checksum != calculateChecksumFromBuffer(buffer,
                sizeof(TelemetryMessageHeader) - checksumStartOffset + dataSize,
                checksumStartOffset)) {
            handler->errorHandler(handler, CHECKSUM_ERROR);
            continue;
        }
        buffer->advance(buffer, sizeof(header) + dataSize);''').replace('\n', '\n' + indentString) + '\n')

    def _getBaseType(self, typeInfo):
        """
        Get the base type name of a type.

        :param typeInfo: The type to get the base type name of.
        :return: The base type as a valid C type.
        """
        return self._BASE_TYPE_C_TYPES[typeInfo.getBaseType(self._database)]

    def _resolveUnitName(self, rawName):
        """
        Add the prefix to the name of the unit if a prefix was specified.

        :param rawName: The raw name of the unit.
        :return: The final name of the unit that should be used in generated code.
        """
        if not self._prefix:
            return rawName
        return f'{self._prefix}_{rawName}'

    def _getUnitVariantName(self, baseUnit, variant):
        """
        Get the name of variant of a base unit.

        :param baseUnit: The base unit.
        :param variant: The variant of the base unit.
        :return: The name to use for the variant.
        """
        variantName = variant.baseTypeName
        offset = 0
        for matchingBlock in SequenceMatcher(
                a=baseUnit.baseTypeName, b=variantName).get_matching_blocks():
            if matchingBlock.size > 1:
                variantName = variantName[:matchingBlock.b - offset] \
                              + variantName[matchingBlock.b - offset + matchingBlock.size:]
                offset += matchingBlock.size
        return self._resolveUnitName(baseUnit.name + variantName.replace('_t', '').capitalize())

    def _formatValue(self, value, typeInfo):
        """
        Format a value to C code.

        :param value: The value to format.
        :param typeInfo: Type information about the value.
        :return: The formatted value.
        """
        isUnit = isinstance(typeInfo, Unit)
        if not isUnit and typeInfo.baseTypeName is not None:
            baseTypeInfo = self._database.getTypeInfo(typeInfo.baseTypeName)
            isUnit = isinstance(baseTypeInfo, Unit)
            typeInfo = baseTypeInfo
        if isUnit:
            baseTypeInfo = self._database.getTypeInfo(typeInfo.baseTypeName)
            return f'{self._getTypeName(typeInfo)}({self._formatValue(value, baseTypeInfo)})'
        if isinstance(value, Enum):
            return self._toScreamingSnakeCase(value.name)
        if isinstance(value, ArrayType):
            return '{' + ', '.join(self._formatValue(child, value.__class__.getElementTypeInfo())
                                   for child in value) + '}'
        if isinstance(value, StructType):
            return '{' + ', '.join(
                f'.{self._toCamelCase(name)} = {self._formatValue(child, value.__class__[name])}'
                for name, child in value.items()) + '}'
        if isinstance(value, bool):
            return repr(value).lower()
        if isinstance(value, bytes):
            return f'"{str(value)[2:-1]}"'
        if isinstance(value, float) and typeInfo is not None \
                and self._getBaseType(typeInfo) == 'float':
            return repr(value) + 'f'
        return repr(value)

    def _writeType(self, file, name, typInfo, knownTypes, indent=0):
        # type: (Any, str, TypeInfo, List[str], int) -> None
        """
        Write the definition for a type to the given file.

        :param file: The file to write the definition to.
        :param name: The name of the type.
        :param typInfo: The type info of the type.
        :param knownTypes: A list of types that have already been written.
        :param indent: The current indent level.
        """
        file.write(self._formatDocumentation(typInfo.description, indent=indent))
        file.write(self._indent('typedef ' if indent == 0 else '', indent))
        baseName = size = None
        if typInfo.baseTypeName is not None:
            baseName, size = self._splitTypeAndSize(typInfo.baseTypeName)
            try:
                baseName = self._getTypeName(self._database.getTypeInfo(baseName))
            except UnknownTypeError:
                pass
        knownType = baseName in knownTypes
        name = self._toCamelCase(name)
        if issubclass(typInfo.type, Enum) and not knownType:
            file.write(self._indent('enum {\n', indent))
            enumValues = list(typInfo.type)
            nameAppendix = ''
            telemetryEnum = self._database.telemetryTypes[0].id.__class__ \
                if self._database.telemetryTypes else None
            try:
                telecommandEnum = self._database.getTypeInfo('TelecommandType').type
            except UnknownTypeError:
                telecommandEnum = None
            if typInfo.type == telemetryEnum:
                nameAppendix = 'Telemetry_'
            elif typInfo.type == telecommandEnum:
                nameAppendix = 'Command_'
            for enumValue in enumValues:
                file.write(self._formatDocumentation(enumValue.__doc__, indent=indent + 1))
                enumValueName = self._toScreamingSnakeCase(nameAppendix + enumValue.name)
                file.write(self._indent(f'{enumValueName} = {enumValue.value},\n', indent + 1))
            file.write('\n')
            nameUpper = self._camelToSnakeCase(name).upper()
            if not enumValues:
                raise CommunicationDatabaseError('Can\'t represent empty enum')
            firstName = self._toScreamingSnakeCase(nameAppendix + enumValues[0].name)
            lastName = self._toScreamingSnakeCase(nameAppendix + enumValues[-1].name)
            file.write(self._indent(f'{nameUpper}_FIRST MAYBE_UNUSED = {firstName},\n', indent + 1))
            file.write(self._indent(f'{nameUpper}_LAST MAYBE_UNUSED = {lastName},\n', indent + 1))
            file.write(self._indent('} ' + name + ';\n', indent))
        elif issubclass(typInfo.type, StructType) and not knownType:
            file.write('struct PACKED {\n')
            for childName, childTypeInfo in typInfo.type:
                self._writeType(file, childName, childTypeInfo, knownTypes, indent=indent + 1)
            file.write(self._indent('} ' + name + ('' if size is None else size) + ';\n', indent))
        else:
            if issubclass(typInfo.type, Enum):
                baseType = self._getBaseType(typInfo)
                file.write(f'TYPED_ENUM({baseName}, {baseType}) {name}{size};\n')
            else:
                file.write(f'{baseName} {name}{size};\n')

    def _writeFormattedTemplate(self, destination, templateName, **kwargs):
        """
        Read a template and write it to the destination file, replacing all formatting variables
        using the provided formatting handlers.
        These handlers will be called with the file as their only argument.

        :param destination: The destination file path.
        :param templateName: The template file name.
        :param kwargs: A mapping of lowercase formatting variable names to format handlers
        """
        formattingHandlers = {
            '%AUTO_GENERATED_NOTICE%': (len('%AUTO_GENERATED_NOTICE%'), lambda file: file.write(
                '// *** AUTOGENERATED FILE - Do not edit manually! '
                '- Changes will be overwritten! *** //')),
            '%INCLUDE_PREFIX%': (len('%INCLUDE_PREFIX%'), lambda file: file.write(
                'generated' if self._prefix is None else f'generated/{self._prefix}'))
        }
        for replacementString, handler in kwargs.items():
            replacementString = f'%{replacementString.upper()}%'
            formattingHandlers[replacementString] = (len(replacementString), handler)
        templateFilePath = os.path.join(self._templateDir, templateName)
        with open(templateFilePath, encoding='utf-8') as templateFile, \
                open(destination, 'w', encoding='utf-8') as destinationFile:
            for line in templateFile:
                formatters = []
                for replacementString, (replacementOffset, formatter) in formattingHandlers.items():
                    try:
                        formatters.append(
                            (line.index(replacementString), replacementOffset, formatter))
                    except ValueError:
                        pass
                lastEndIndex = 0
                for startIndex, offset, formatter in sorted(formatters, key=lambda item: item[1]):
                    destinationFile.write(line[lastEndIndex:startIndex])
                    formatter(destinationFile)
                    lastEndIndex = startIndex + offset
                destinationFile.write(line if lastEndIndex == 0 else line[lastEndIndex:])

    def _formatDocumentation(self, description, indent=0, forceMultiline=False):
        """
        Format the given description as a documentation comment.

        :param description: A description.
        :param indent: The current indent.
        :param forceMultiline: Whether to force the comment to become a multiline comment.
        :return: A formatted documentation comment.
        """
        if not description:
            return ''
        indentStr = self._indent('', indent)
        docIndentStr = f'{indentStr} * '
        documentation = self._formatLines(
            description, docIndentStr, startIndentLength=len(docIndentStr))
        if not forceMultiline and len(documentation) < self._MAX_LINE_LENGTH - len(indentStr) - 6 \
                and '\n' not in documentation:
            return f'{indentStr}/** {documentation} */\n'
        return f'{indentStr}/**\n{docIndentStr}{documentation}\n{indentStr} */\n'

    def _getPrintFormat(self, typeInfo):
        """
        Get the printf format for a given type.

        :param typeInfo: A type info.
        :return: The format specifier for the given type.
        """
        return self._BASE_TYPE_PRINT_FORMATS[typeInfo.getBaseType(self._database)]

    def _getTypeName(self, typeInfo):
        """
        :param typeInfo: The type to get the name for.
        :return: The name for a given type, as a valid C identifier.
        """
        if isinstance(typeInfo, Unit):
            baseUnit = self._database.units[typeInfo.name][0]
            if typeInfo is baseUnit:
                return self._resolveUnitName(typeInfo.name + '_t')
            return self._getUnitVariantName(baseUnit, typeInfo) + '_t'
        try:
            baseType = TypeInfo.BaseType(typeInfo.name)
        except ValueError:
            pass
        else:
            return self._BASE_TYPE_C_TYPES[baseType]
        if issubclass(typeInfo.type, ArrayType):
            try:
                baseType = TypeInfo.BaseType(typeInfo.getBaseType(self._database))
                return typeInfo.name.replace(f'{baseType.value}[', self._BASE_TYPE_C_TYPES[baseType] + '[')
            except ValueError:
                pass  # Ignore types without a base type
        return typeInfo.name

    def _formatArgumentType(self, typeInfo):
        """
        Format the type as an arguments type.
        Converts structs and arrays to const pointers.

        :param typeInfo: The type information.
        :return: A string representing the type as an argument.
        """
        typeName = self._getTypeName(typeInfo)
        if issubclass(typeInfo.type, StructType):
            typeName = f'const {typeName}*'
        elif issubclass(typeInfo.type, ArrayType):
            bracketStart = typeName.find('[')
            typeName = f'const {typeName[:bracketStart]}*'
        return typeName

    def _formatVariableDeclaration(self, typeInfo, name, useEnumBaseType=False):
        """
        Format the type as a variable declaration.

        :param typeInfo: The type information.
        :param name: The name of the variable.
        :param useEnumBaseType: If the type is an enum, use it's base type.
        :return: A string representing the type as an argument.
        """
        typeName = self._getTypeName(typeInfo)
        if issubclass(typeInfo.type, ArrayType):
            bracketStart = typeName.find('[')
            return f'{typeName[:bracketStart]} {name}{typeName[bracketStart:]}'
        elif useEnumBaseType and issubclass(typeInfo.type, Enum):
            typeName = self._getBaseType(typeInfo)
        return f'{typeName} {name}'

    @staticmethod
    def _generateFunctionDocumentation(functionDoc, arguments, returnDoc=None):
        """
        Generate a function documentation.

        :param functionDoc: A description of the function.
        :param arguments: Argument names and descriptions.
        :param returnDoc: A description of the return value if there is one.
        :return: A documentation for the function.
        """
        argumentDoc = ''
        for argumentName, argumentDescription in arguments.items():
            if argumentDescription is not None:
                argumentDoc += f'\n@param {argumentName} {argumentDescription}'
        if argumentDoc or returnDoc:
            functionDoc += '\n' + argumentDoc
        if returnDoc is not None:
            functionDoc += f'\n@return {returnDoc}'
        return functionDoc

    def _generateSizeStaticAssertion(self, file, typ, indent=0):
        """
        Generate a `static_assert` that asserts the given type is an expected size. This is only
        necessary for float and double types, because their size isn't standardized in C.

        :param file: The file to write the generated assertion to.
        :param typ: The type whose size should be asserted.
        :param indent: The indentation of the assertion statement.
        """
        if issubclass(typ.type, float):
            indentStr = self._indent('', indent)
            baseType = self._getBaseType(typ)
            file.write(self._formatLines(
                f'{indentStr}static_assert(sizeof({baseType}) == {typ.getSize(self._database)}, '
                f'"{baseType} has an invalid size on this platform and can not be used");',
                continueIndent=indentStr + '    ') + '\n')

    @staticmethod
    def _formatLines(lines, continueIndent, startIndentLength=0, maxLineLength=_MAX_LINE_LENGTH):
        """
        Format the given lines, so they don't exceed the maximum line length.

        :param lines: The lines to format.
        :param continueIndent: The string to use as indent for continued lines.
        :param startIndentLength: The length of the starting indent.
        :param maxLineLength: The maximum length of a line in characters.
        :return: The formatted lines.
        """
        formattedLines = []
        linesList = lines.split('\n')
        indentStr = ''
        indentLength = startIndentLength
        formatIndentationLevel = 0
        while linesList:
            line = ' ' * formatIndentationLevel + linesList.pop(0).rstrip()
            maxLength = maxLineLength - indentLength - formatIndentationLevel
            if len(line) > maxLength:
                for separator in [': ', '. ', ', ', ' ']:
                    firstPart, *parts = line.split(separator)
                    if len(firstPart) > maxLength:
                        continue
                    line = firstPart
                    parts = list(parts)
                    while parts and len(line) + len(separator) + len(parts[0]) \
                            + len(separator.rstrip()) <= maxLength:
                        line += separator + parts[0]
                        parts = parts[1:]
                    if parts:
                        if formatIndentationLevel == 0 and line.startswith('@'):
                            words = 0
                            lastCharWasWord = False
                            for i, character in enumerate(line):
                                if character.isspace():
                                    if lastCharWasWord:
                                        words += 1
                                    lastCharWasWord = False
                                else:
                                    lastCharWasWord = True
                                    if words == 2:
                                        formatIndentationLevel = i
                                        break
                        linesList.insert(0, separator.join(parts))
                        line += separator.rstrip()
                    else:
                        formatIndentationLevel = 0
                    break
                else:
                    linesList.insert(0, line[maxLength:])
                    line = line[:maxLength]
            else:
                formatIndentationLevel = 0
            formattedLines.append((indentStr + line).rstrip())
            indentStr = continueIndent
            indentLength = len(indentStr)
        return '\n'.join(formattedLines)

    @staticmethod
    def _splitTypeAndSize(typ):
        """
        Split a type specifier in base type and optional array size.

        :param typ: The type specifier.
        :return: The base type and optional array size.
        """
        bracketIndex = typ.find('[')
        if bracketIndex != -1:
            size = typ[bracketIndex:]
            if size.startswith('[.'):
                size = '[0]'
            return typ[:bracketIndex], size
        return typ, ''

    @staticmethod
    def _indent(text, indent):
        """
        :param text: A text.
        :param indent: An indent level.
        :return: The text with the given indent applied.
        """
        return ' ' * 4 * indent + text

    @staticmethod
    def _snakeToCamelCase(name):
        """
        Convert a name from snake_case to camelCase.

        :param name: The name to convert.
        :return: The converted name.
        """
        return ''.join(word.capitalize() for word in name.split('_'))

    @classmethod
    def _camelToSnakeCase(cls, name):
        """
        Convert a name from camelCase to snake_case.

        :param name: The camel case name.
        :return: The converted name.
        """
        return cls._SNAKE_CASE_PATTERN.sub('_', name).lower()

    @staticmethod
    def _toCamelCase(words):
        """
        Convert a words to a camel case name.

        :param words: The words to convert.
        :return: A camel case name from the words.
        """
        words = words.split()
        return words[0] + ''.join(word[0].upper() + word[1:] for word in words[1:])

    @staticmethod
    def _toScreamingSnakeCase(name):
        """
        :param name: The name to convert.
        :return: The name converted to SCREAMING_SNAKE_CASE.
        """
        return '_'.join(part.upper() for part in name.split())


class BaseCodeGenerator(CCodeGenerator):
    def _generateReceiverHeader(self, generatedIncludeDir):
        """
        Generate a header that defines a handler type for received messages.

        :param generatedIncludeDir: The path to the 'include' directory for generated content.
        :return: Information about the message handlers.
        """
        messageHandlers = OrderedDict()
        if self._database.telemetryTypes:
            self._writeFormattedTemplate(
                os.path.join(generatedIncludeDir, 'receiver.h'), 'receiver.h',
                message_handlers=lambda file: self._writeReceiverHandlerDefinitions(file, messageHandlers),
            )
        return messageHandlers

    def _writeReceiverHandlerDefinitions(self, outputFile, messageHandlers):
        """
        Write the code to define prototypes for all message handler functions to the file.

        :param outputFile: The file to write the message handler functions prototypes to.
        :param messageHandlers: An OrderedDict that will be filled with information about the message handlers.
        """
        isFirstLine = True
        for telemetry in self._database.telemetryTypes:
            handlerFunction = 'handleMessage' + self._snakeToCamelCase(telemetry.id.name)
            handlerArguments = OrderedDict()
            for dataPoint in telemetry.data:
                typeName = self._formatArgumentType(dataPoint.type)
                handlerArguments[self._toCamelCase(dataPoint.name)] = typeName, dataPoint
            messageHandlers[handlerFunction] = (telemetry, handlerArguments)
            if isFirstLine:
                isFirstLine = False
            else:
                outputFile.write('\n\n    ')
            outputFile.write(self._formatDocumentation(self._generateFunctionDocumentation(
                f'Handle a {telemetry.id.name} message.', OrderedDict(
                        [('handler', 'The message handler.')] +
                        [(self._toCamelCase(dataPoint.name), dataPoint.description) for dataPoint in telemetry.data])),
                indent=1)[4:])
            if not handlerArguments:
                handlerArgumentsStr = 'void'
            else:
                handlerArgumentsStr = 'struct MessageHandler* handler, ' + ', '.join(
                    f'{typ} {name}' for name, (typ, _) in handlerArguments.items())
            outputFile.write(self._formatLines(f'    void (*{handlerFunction})({handlerArgumentsStr});',
                                               continueIndent='        '))

    def _generateReceiverSource(self, generatedSourceDir, messageHandlers):
        if messageHandlers:
            self._writeFormattedTemplate(
                os.path.join(generatedSourceDir, 'receiver.c'), 'receiver_base.c',
                message_handlers=lambda file: self._writeReceiverHandlers(file, messageHandlers),
            )

    def _writeReceiverHandlers(self, outputFile, messageHandlers):
        """
        Write the code to parse messages and call the corresponding handlers to the file.

        :param outputFile: The file to write the message parser code to.
        :param messageHandlers: An OrderedDict with information about the message handlers.
        """
        isFirstLine = True
        for handlerFunction, (telemetry, handlerArguments) in messageHandlers.items():
            if isFirstLine:
                isFirstLine = False
            else:
                outputFile.write('\n        ')
            outputFile.write(f'case TELEMETRY_{telemetry.id.name}: {{\n')
            self._writeMessageArgumentsParser(outputFile, telemetry, handlerArguments)
            argumentsStr = ', '.join(['handler', *handlerArguments])
            outputFile.write(f'            handler->{handlerFunction}({argumentsStr});\n')
            outputFile.write('            return true;\n')
            outputFile.write('        }')

    def _generateTelemetryHeader(self, generatedIncludeDir):
        return OrderedDict()


def main():
    """
    Generate C code source code that can parse and serialize messages
    from the current state of the communication database.

    :return: The program exit status to indicate whether code generation was successful.
    """
    parser = ArgumentParser(description='Generate C code from the shared communication database')
    parser.add_argument('sourceDir', help='path to directory where the C header code is stored')
    parser.add_argument('headerDir', help='path to directory where the C source code is stored')
    parser.add_argument('-d', '--database', required=True,
                        help='path to directory where the database files are stored')
    parser.add_argument('-p', '--prefix',
                        help='A prefix that will be applied to the generated code')
    parser.add_argument('--forBase', action='store_true', help='Generate code for the base')
    arguments = parser.parse_args()
    try:
        database = CommunicationDatabase(arguments.database)
    except CommunicationDatabaseError as error:
        print(f'[Error] Failed to read database: {error}')
        return 1
    generator = (BaseCodeGenerator if arguments.forBase else CCodeGenerator)(
        database, arguments.prefix)
    try:
        generator.generate(arguments.sourceDir, arguments.headerDir)
    except IOError as error:
        print(f'[Error] Failed to generate code: {error}')
        return 1
    print('Code generation successful')


if __name__ == '__main__':
    sys.exit(main())
