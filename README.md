# ECom (Efficient Communication Library)
[![Documentation Status](https://readthedocs.org/projects/ecom/badge/?version=latest)](https://ecom.readthedocs.io)

A library that provides a way to define a communication between devices.
Both high level platforms (via a Python module) and a low-level embedded platforms,
like an Arduino (via generated C code) are supported.
The communication is as efficient as possible with a focus on minimizing the size of the transmitted
payload, and minimizing memory usage and copy operations on the embedded device. It supports
variable sized messages, where the size is included as part of the message. 

Communication between tow high level platforms (Python to Python), one high level and one low level platform
(Python to embedded device) and between two low level platforms (embedded to embedded) are all supported.

The library was originally developed to be used in the [ASTER project](https://aster-rexus.com).

Online documentation is available [here](https://ecom.readthedocs.io).

## Terminology
The communication happens between two sides. One of these sides is considered the `base` and the
other side is considered the `secondary device`. The base is usually controlling the secondary
device to some extent. In the typical groundstation and satellite case, the groundstation is the
base and the satellite represents the secondary device.

Within the ECom system, messages from the base to the secondary device are called `Telecommands`.
Messages from the secondary device to the base are called `Telemetry`.

## Examples
Some examples demonstrating the usage of the library can be found in the [`examples`](./examples)
directory. This includes an [example communication database](./examples/database) with further
documentation on the database.

## Library

The [`ecom`](ecom) Python module provides access to a communication database (see [example](./examples/database)).
It allows to parse and serialize messages defined in the communication protocol.
It also includes a [code generator named `ecomUpdate`](scripts/ecomUpdate.py) to generate a serializer and parser in C.

### Installation
The `ecom` library can be installed with pip, this requires [git](https://git-scm.com/) to be installed:
```shell
pip install git+https://gitlab.com/team-aster/software/ecom
```

Alternatively, if the repository is already downloaded locally, it can be installed with the following command:
```shell
pip install .
```

This will cause the `ecomUpdate` script to be added to the `PATH`, so it can be called from everywhere.

### Usage

The library provides a parser and serializer. In case the data is both parsed and serialized,
a `ResponseParser` should be used, as it can handle deserialization of telecommand responses.  

##### Parsing
```python
from ecom.database import CommunicationDatabase
from ecom.parser import TelemetryParser

# Initialization
database = CommunicationDatabase('/path/to/database')
parser = TelemetryParser(database)
connection = ...

def handleParseError(error):
    print(f'Parsing error: {error}')

# Read loop
for data in connection.read():
    for telemetry in parser.parse(data, errorHandler=handleParseError):
        print(telemetry)
```

Note: The parser provides an `errorHandler` argument, which should always be provided.
Otherwise, the parser will throw an exception when it encounters a parsing error,
which causes it to ignore all data after the parsing error that was given to that call of `parse`.

##### Serializing

```python
from ecom.database import CommunicationDatabase
from ecom.serializer import TelecommandSerializer

# Initialization
database = CommunicationDatabase('/path/to/database')
serializer = TelecommandSerializer(database)
connection = ...

# Send a telecommand
motorEnabledCommand = database.getTelecommandByName('MOTOR_ENABLED')
telecommandBytes = serializer.serialize(motorEnabledCommand, enabled=True)
connection.write(telecommandBytes)
```

#### Database verification

The following command will attempt to load the database and can be used to verify the database.

```shell
python ecom/database.py --dataDir /path/to/database
```

#### Code generation

The code generation scripts expect a path to a directory which should contain the source code
and a path to a directory which should contain the header files. It will generate a `generated`
directory in both and write the generated code into them.
Invoking the following command will generate the C code in the
`path/to/include/directory/generated` and `path/to/source/directory/generated` directory.

```shell
ecomUpdate --dataDir /path/to/database path/to/source/directory path/to/include/directory
```

#### Unit tests

The unit tests for the module can be run as follows:

```shell
python -m unittest discover tests
```

## Uses

This library has been successfully used in student projects:

* The library was used for communication between the hardware on the rocket and the groundstation in the ASTER project,
  which flew on a REXUS rocket on the 21st March 2023.
* The library was also used for communicating between Arduino microcontrollers on the 2023 balloon projects
  of the M2TSI master of the university in Toulouse.
