from abc import ABC, abstractmethod
from typing import Any, Dict, Optional

from ecom.datatypes import TypeInfo, StructType
from ecom.message import MessageType


class VerificationError(RuntimeError):
    """ An error indicating that a message failed its verification check. """


class MissingDataForVerificationError(VerificationError):
    """ An error indicating that a message failed its verification check because some it is incomplete. """


class MessageVerifier(ABC):
    """ A helper that can add data to verify the integrity of a message and use that data to verify a message. """
    @abstractmethod
    def verify(self, data: bytes, message: MessageType,
               header: TypeInfo[StructType], messageSize: int) -> Optional[bytes]:
        """
        Verify the integrity of the message.

        :raises VerificationError: if the message fails the verification check and the original data can't be recovered.
        :param data: The raw data of the message. May also include other trailing data.
        :param message: The type of message.
        :param header: The header type information of the message.
        :param messageSize: The size of the message, as calculated with the unverified data.
        :return: None if the message was verified successfully or the bytes of the error corrected message
                 and any bytes from the given data that were not part of the message.
        """

    @abstractmethod
    def addVerificationData(self, data: bytes, message: MessageType, header: TypeInfo[StructType]) -> bytes:
        """
        Add extra verification data to a message.

        :param data: The message data.
        :param message: The type of message.
        :param header: The header type information of the message.
        :return: The message with verification data.
        """

    @abstractmethod
    def addPlaceholderVerificationData(self, data: Dict[str, Any]):
        """
        When constructing a message, this allows to add a placeholder value to the header
        that will be filled later by `addVerificationData`.

        :param data: A dictionary to store the placeholder verification data to.
        """
