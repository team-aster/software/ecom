# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

from typing import Any

from docutils import nodes
from sphinx import addnodes
from sphinx.transforms.post_transforms import ReferencesResolver
from pygments import lexers
from pygments.lexer import RegexLexer, bygroups
from pygments.token import Keyword, Literal, Name, Operator, Punctuation

BASE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
sys.path.insert(0, BASE_DIR)

# -- Project information -----------------------------------------------------
from ecom import __version__

project = 'ECom'
copyright = '2022, Aster'
author = 'Aster'

# The full version, including alpha/beta/rc tags
release = version = __version__

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.viewcode',
    'myst_parser',
    'autoapi.extension',
]

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

autoapi_dirs = [os.path.join(BASE_DIR, 'ecom')]
autoapi_options = [
    'members',
    'undoc-members',
    'show-inheritance',
    'show-module-summary',
    'special-members',
    'imported-members',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# Suppressed warnings
suppress_warnings = [
    'myst.header',  # We want to be able to add smaller headers for styling.
]

myst_heading_anchors = 3

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = []

html_show_sourcelink = False

# -- Options for EPUB output -------------------------------------------------
epub_show_urls = 'footnote'


# -- Customizations ----------------------------------------------------------
__all__ = ['CsvLexer']


class CsvLexer(RegexLexer):
    """ Simple CSV lexer for Pygments. """

    name = 'Csv'
    aliases = ['csv', 'comma-separated', 'comma-separated-values']
    filenames = ['*.csv']

    tokens = {
        'root': [
            (r'^[^,\n]*', Operator, 'second'),
        ],
        'second': [
            (r'(,)([^,\n]*)', bygroups(Punctuation, Name.Constant), 'third'),
        ],
        'third': [
            (r'(,)([^,\n]*)', bygroups(Punctuation, Keyword.Declaration), 'fourth'),
        ],
        'fourth': [
            (r'(,)([^,\n]*)', bygroups(Punctuation, Literal.Number), 'fifth'),
        ],
        'fifth': [
            (r'(,)([^,\n]*)', bygroups(Punctuation, Literal.String.Single), 'sixth'),
        ],
        'sixth': [
            (r'(,)([^,\n]*)', bygroups(Punctuation, Name.Constant), 'seventh'),
        ],
        'seventh': [
            (r'(,)([^,\n]*)', bygroups(Punctuation, Keyword.Namespace), 'eighth'),
        ],
        'eighth': [
            (r'(,)([^,\n]*)', bygroups(Punctuation, Literal.Number), 'ninth'),
        ],
        'ninth': [
            (r'(,)([^,\n]*)', bygroups(Punctuation, Literal.String.Single), 'tenth'),
        ],
        'tenth': [
            (r'(,)([^,\n]*)', bygroups(Punctuation, Keyword.Type), 'unsupported'),
        ],
        'unsupported': [
            (r'(.+)', bygroups(Punctuation)),
        ],
    }


class SourceReferenceResolver(ReferencesResolver):
    """ A reference resolver that converts all unresolved references to sources into links to the GitLab repository. """
    default_priority = 1
    gitUrl = 'https://gitlab.com/team-aster/software/ecom/-/tree/main/'

    def run(self, **kwargs: Any) -> None:
        for node in self.document.findall(addnodes.pending_xref):
            if node['reftype'] != 'myst':
                continue
            path = os.path.join(os.path.dirname(node.source), node['reftarget'])
            if not path.endswith('.md') and os.path.exists(path):
                link = self.gitUrl + os.path.relpath(path, BASE_DIR).replace(os.path.sep, '/')
                contentNode = node[0].deepcopy()
                reference = nodes.reference()
                reference.source = node.source
                reference.line = node.line
                reference['refuri'] = link
                title = node.get('title')
                if title is not None:
                    reference['title'] = title
                reference.append(contentNode)
                node.replace_self(reference)


def setup(app):
    getattr(lexers, '_mapping').LEXERS['CsvLexer'] = ('docs.conf', 'Csv', ('csv',), ('*.csv',), ('text/csc',))
    app.add_post_transform(SourceReferenceResolver)
