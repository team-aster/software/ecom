# Telecommand arguments

[Telecommands](commands.md) can optionally have arguments. They are declared by providing a file
with the same name as the command and the ending `.csv` in the `commandArguments` subdirectory of the communication
database (see [example](../../examples/database/commandArguments)).

Each file consists of a csv table with four columns and one header row containing the name of each
column. Each other row declares one telecommand argument:

| Column name | Column description                                                                                                          |
|-------------|-----------------------------------------------------------------------------------------------------------------------------|
| Name        | The name of the telecommand argument.                                                                                       |
| Type        | The type of the data that can be sent via this argument. It can be a [unit](units.md) or [shared](sharedDataTypes.md) type. |
| Default     | An optional default value for this argument. This is used in the serializer, if the value was not provided.                 |
| Description | An optional human readable description of this argument.                                                                    |

The following example defines an argument for a `CALIBRATE_IMU` command and defines one argument,
`imu` which is an unsigned 8-bit integer, without a default value.

Filename: `CALIBRATE_IMU.csv`
```csv
Name,Type,Default,Description
imu,uint8,,The IMU to calibrate.
```

Using all defined arguments, the database will automatically create a constant named
`MAX_TELECOMMAND_DATA_SIZE`, which will contain the maximum size in bytes needed to store the
serialized version of their arguments for all commands. It is the maximum amount of bytes needed
for any command to transmit its arguments.
