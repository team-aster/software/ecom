# Shared constants

The communication database allows to specify constants that are the same for the receiver and
sender. This is intended to share constants which are used in the payload definition.
These constants can be used as default values and array size specifiers in the payload descriptions.

The shares constants are defined in the [`sharedConstants.csv` file](../../examples/database/sharedConstants.csv).
It consists of a csv table with 4 columns and one header row,
which contains the names of the columns:

| Column name  | Column description                                                             |
|--------------|--------------------------------------------------------------------------------|
| Name         | The name of the shared constant.                                               |
| Value        | The value of the shared constant. The value is interpreted based on the type.  |
| Type         | The type of the shared constant. A [unit type](units.md) can be used here.     |
| Description  | A human readable description of the shared constant.                           |
