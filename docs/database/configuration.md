# Configuration

The configuration system allows to define named values with defaults that can be changed and
queried while the secondary device is running with a telecommand. The system has two components.


## Configuration definition

The configurations are defined in the [`configuration.csv` file](../../examples/database/configuration.csv).
It consists of a csv table with four columns and one header row, which contains the name of the columns.
Each row represents one configuration item:

| Column name    | Column description                                                                                                               |
|----------------|----------------------------------------------------------------------------------------------------------------------------------|
| Name           | The name of the configuration item.                                                                                              |
| Type           | The type that is used to represent this configuration item. Can be a [unit](units.md) or [shared data](sharedDataTypes.md) type. |
| Default Value  | The default value of this configuration item.                                                                                    | 
| Description    | An optional human readable description of this configuration item.                                                               |

The following example defines two configuration items: A boolean `imu enabled`, which is enabled
by default, and a `sensor check interval`, which is `5000` milliseconds by default, using a
`ms` [unit](units.md).

```csv
Name,Type,Default Value,Description
imu enabled,bool,true,Wheter the IMU is enabled.
sensor check interval,ms,5000,How much time to wait between checking the sensor.
```

Using the configuration items specified in the [`configuration.csv`](../../examples/database/configuration.csv) file,
the database will automatically add the following constants:

* `NUM_CONFIGURATIONS`:
  The number of provided configuration items. In the example above, it would be the number `2`.
* `DEFAULT_CONFIGURATION`:
  A struct that contains the default configuration of the declared config items. For the example
  above, represented as a json object, it would be
  ```json
  {
    "imu enabled": true,
    "sensor check interval": 5000
  }
  ```
* `MAX_CONFIG_VALUE_SIZE`:
  The maximum size in bytes of any configuration value. In the example above,
  it would be the maximum between `1` (the size of `bool`) and the size of the `ms` unit. 

Additionally, the following types will be defined:

* `ConfigurationId`: An enum type containing an enum value for each configuration item.
* `Configuration`: A struct type that contains a child for each configuration item. This is the
  type used in the `DEFAULT_CONFIGURATION` constant,
  and it can be used to hold a configuration state.


## Configuration telecommands

If a [command](commands.md) is defined with one argument of type `ConfigurationId` and a return
value of the special type `config?`, the database will mark this command as a configuration getter
telecommand. An automatic handler for this telecommand will be generated in the C generator, which
responds to this telecommand with the value of the configuration indicated by the `ConfigurationId`
argument. A `ResponseParser` is able to automatically convert the response to the correct data type.
The automatically generated C code requires the following function to be defined somewhere and to
provide access to the current configuration:

```c
const Configuration* getConfig();
```

Also, if a [command](commands.md) is defined with no return types and two arguments, one of type
`ConfigurationId` and another one of the special type `config?`, the database will mark this command
as a configuration setter telecommand. An automatic handler for this telecommand will be generated
in the C generator, which correctly parses the configuration value to its correct data type and
updates the configuration with the new value. The automatically generated C code requires two
functions to be defined somewhere:

A function to get a mutable configuration object and indicate that a change will be made.
```c
Configuration* beginConfigUpdate();
```

And a function to commit the changes made to the mutable configuration object retrieved
with `beginConfigUpdate`.
```c
void commitConfigUpdate();
```
