# Telemetry arguments

Each [telemetry type](telemetry.md) is expected to have a file in the `telemetryArguments` subdirectory
of the communication database (see [example](../../examples/database/telemetryArguments)),
which describes the data send for the specific telemetry type.
The file must be named like the telemetry type with a `.csv` ending. It consists of
a csv table with three columns. The first row is a header which contains the names of each column.
Every other row defines a data point which is sent for this telemetry type:

| Column name | Column description                                                                                                        |
|-------------|---------------------------------------------------------------------------------------------------------------------------|
| Name        | The name of this telemetry data point.                                                                                    |
| Type        | The type of data this telemetry data point represents. It can be a [unit](units.md) or [shared](sharedDataTypes.md) type. |
| Description | An optional human readable description of the telemetry data point.                                                       |

The following example shows the telemetry arguments for a `RESPONSE` telemetry type. It specifies
that when sending this telemetry it will include an unsigned 8-bit number representing a
`command number` and an array of bytes as `value`, whose size depends on the shared variable
`MAX_TELECOMMAND_RESPONSE_SIZE`.

Filename: `RESPONSE.csv`
```csv
Name,Type,Description
command number,uint8,The counter number of the command that generated this response.
value,bytes[MAX_TELECOMMAND_RESPONSE_SIZE],The response value of the command.
```
