# Units

The communication database allows to specify units to be used in communication payloads.
These units are named wrappers around a [base type](baseTypes.md).
They are declared in the [`units.csv` file](../../examples/database/units.csv).
The file consists of a csv table with 3 columns and a heading row,
containing the names of each column:

| Colum name   | Column description                                                                             |
|--------------|------------------------------------------------------------------------------------------------|
| Name         | The name of the unit type. This name can be used where a type is required.                     |
| Type         | The underlying [base type](baseTypes.md) of this unit. Only raw types, no arrays are accepted. |
| Description  | A human readable description of this unit.                                                     |

## Variants

Units can have variants, which are datatypes representing the same unit but with a different underlying data type.
In the communication database, these don't have to be a defined explicitly in the `units.csv` file.
Whenever it is possible to use a unit, one can instead give a base type with one of the defined units in parentheses.

As an example, given the following `units.csv` file, which defines a `unit1` unit:

```csv
Name,Type,Description
unit1,double,A demonstration unit.
```

One can define and use a variant, for example as a telemetry argument:

```csv
Name,Type,Description
datapoint1,float (unit1),Some datapoint for the demonstration.
```

These variants show up in the `units` dictionary of the communication database as additional units with different
base types in the list after the base unit.

```pycon
>>> from ecom.database import CommunicationDatabase
>>> database = CommunicationDatabase(...)
>>> database.units['volt']
[
    Unit(type=<class 'float'>, name='volt', baseTypeName='double', description='Voltage.', default=None),
    Unit(type=<class 'float'>, name='volt', baseTypeName='float', description='Voltage.', default=None),
]
```

On the embedded side, the two units are two different types:
```c
volt_t baseUnitValue = volt_t(0);
voltFloat_t variantUnitValue = voltFloat_t(0);
```
