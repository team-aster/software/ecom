# Telecommands

Telecommands are packages of data that can be sent from the base to the secondary device.
They can have zero, one or multiple arguments, which are additional data that is sent alongside the
command. The telecommand arguments are further detailed in the
[telecommand arguments documentation](commandArguments.md).

The telecommands can also optionally have a return value, in which case the secondary device is
expected to respond with a `RESPONSE` [telemetry package](telemetry.md) which contains the return
value. If no return value is specified, the secondary device is expected to respond with an
`AKNOWLEDGE` [telemetry package](telemetry.md).


## Definition

Telecommands are defined in the [`commands.csv` file](../../examples/database/commands.csv).
The file consists of a csv table with six columns and one header row containing the name of each column.
Each other row declares one telecommand:

| Column name          | Column description                                                                                                                                        |
|----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| Name                 | The name of this telecommand.                                                                                                                             |
| Debug                | Either `true` or `false`, indicates whether this command is only available if debugging commands are allowed ([details](#required-functions)).            |
| Description          | An optional human readable description of the telecommand.                                                                                                | 
| Response name        | A name of the return value, if this command has one. Otherwise it can be left empty.                                                                      |
| Response type        | The type of the return value, if this command has one. Otherwise it can be left empty. It can be a [unit](units.md) or [shared](sharedDataTypes.md) type. |
| Response description | An optional human readable description of the return value.                                                                                               |

The following example defines two telecommands, one telecommand to enable a motor, and one
telecommand to query the enabled state of the motor. The last command can only be used,
if the debug telecommands are enabled.

```csv
Name,Debug,Description,Response name, Response type,Response description
ENABLE_MOTOR,false,Enable the motor.,,,
GET_MOTOR_ENABLED,true,Get whether the motor is enabled.,enabled,bool,The enabled state of the motor.
```

Using the declared telecommands, the database automatically generates a `TelecommandType` enum type
which contains an identifier value for all telecommands.


## Structure

All telecommands are serialized into the format of the special `TelecommandMessageHeader`
[shared data type](sharedDataTypes.md).
If the command has any arguments, they are attached to the end of the header and are
also included in the generated checksum.


## C Code generation

The C code generator will generate a parser which can parse serialized telecommands and calls
the correct handler function for it. The parser declares the following function to feed received
data into the parser:

```c
bool parseMessage(BufferAccessor* buffer, MessageHandler* handler);
```

This function will call the appropriate command handler that is given via the `MessageHandler` object.
It is a collection of function pointers, defining a command handler function for each telecommand
([config commands](configuration.md#configuration-telecommands) are the only exception).
The implementation of these handlers must be provided by the secondary devices code.


### Required functions

The generated C code expects the following functions to be present:

* areDebugCommandsEnabled:
  This function is called when a telecommand is received whose debug attribute is `true`.
  The handler for the command is only executed if this function returns `true`.
  ```c
  /**
   * @note This function is only called when a debug command is received.
   * @note This function has to be provided by the ECom user.
   * @return Whether executing debug commands is currently enabled.
   */
  bool areDebugCommandsEnabled(void);
  ```
* printDebug:
  Used to print a formatted debugging message.
  ```c
  /**
   * Print the formatted debug message.
   *
   * @note This function has to be provided by the ECom user.
   * @param format The format string.
   * @param ... Format values.
   */
  void printDebug(const char* format, ...);
  ```
* printDebug:
  Used to print a formatted error message.

  *Note:* This function is only required when a
  [config command](configuration.md#configuration-telecommands) is defined.
  ```c
  /**
   * Print the formatted error message.
   *
   * @note This function has to be provided by the ECom user.
   * @param format The format string.
   * @param ... Format values.
   */
  void printError(const char* format, ...);
  ```
