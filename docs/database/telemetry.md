# Telemetry

Telemetry is data that the secondary device can send to the base. It encapsulates
housekeeping data and responses to [commands](commands.md)
(see [Special Telemetry](#special-telemetry)).


## Definition

The types of telemetry are defined in the [`telemetry.csv` file](../../examples/database/telemetry.csv).
It contains a csv table with two columns and one heading row that contains the name of the columns.
Every other row defines a new telemetry type. The file does not contain the contents of the telemetry,
this is covered in the [telemetry arguments documentation](telemetryArguments.md).

| Column name | Column description                                            |
|-------------|---------------------------------------------------------------|
| Name        | The name of the telemetry type.                               |
| Description | An optional human readable description of the telemetry type. |

The following example defines an `ACKNOWLEDGE` and `RESPONSE` telemetry type
with their documentation.

```csv
Name,Description
RESPONSE,A response to a telecommand with the response data.
ACKNOWLEDGE,Acknowledge receiving a command.
```


### Special Telemetry

The `ResponseTelemetryParser` expects two telemetry types to be defined:
* `RESPONSE`:
  A telemetry that will be sent back as a response to a telecommand, which contains the result of
  that telecommand.
* `ACKNOWLEDGE`:
  A telemetry that will be sent back as a response to a telecommand, if the telecommand doesn't have
  a return value. It is used to acknowledge the retrieval of that telecommand.


## Structure

All telecommands are serialized into the format of the special `TelemetryMessageHeader`
[shared data type](sharedDataTypes.md) shared data type. The messages are of variable sizes,
dependent on the telemetry type. The serialized data is attached to the end of the header and is
also included in the generated checksum.


## C Code generation

The C code generator will generate a serializer which can serialize data into telemetry packages.
The parser declares a `sendTelemetryName` function where `Name` is replaced by the name of each
telemetry type. Each function accepts the data defined for that telemetry type in the
[telemetry arguments documentation](telemetryArguments.md). The serializer will call a
`writeTelemetry` function, which is expected to be defined somewhere:

```c
bool writeTelemetry(const void* data, size_t dataLength);
```
