# Communication database

```{eval-rst}
.. toctree::
   :maxdepth: 1
   :glob:

   *
```

The communication database specifies the communication between to devices.
It is spread out over multiple files and folders:

* [commands.csv](commands.md) [(Example)](../../examples/database/commands.csv):
  Contains a list of all telecommands and describes an optional return value.
* [commandArguments](commandArguments.md) [(Example)](../../examples/database/commandArguments):
  This folder contains the arguments for the telecommands. If a telecommand has arguments,
  they must be in a file with the same name as the telecommand.
* [telemetry.csv](telemetry.md) [(Example)](../../examples/database/telemetry.csv):
  A list of possible types of telemetry messages.
* [telemetryArguments](telemetryArguments.md) [(Example)](../../examples/database/telemetryArguments):
  This folder contains the definition of the data that is sent with each telemetry type.
  The definition must be in a file with the same name as the telemetry type.
* [sharedConstants.csv](sharedConstants.md) [(Example)](../../examples/database/sharedConstants.csv):
  Constant values that are used in the communication.
* [sharedDataTypes.json](sharedDataTypes.md) [(Example)](../../examples/database/sharedDataTypes.json):
  A description of data types that are used in the communication.
* [configuration.csv](configuration.md) [(Example)](../../examples/database/configuration.csv):
  A description of configuration parameters that can be changed with the configuration telecommand.
* [units.csv](units.md) [(Example)](../../examples/database/units.csv):
  A description of units and their base datatypes used in the communication.
