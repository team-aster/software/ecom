# Log converter

This example demonstrates a utility which reads raw unprocessed telemetry from a file
and converts it into a csv file, while allowing for some filtering of the telemetry.


## Usage 

If just called with a path to the database and binary telemetry file, the tool will parse all telemetry
in the file and print the resulting csv to stdout:
```shell
logConverter.py path/to/communication/database path/to/binary/telemetry.bin
```

To save the csv into a file, specify a path to the output file as follows:
```shell
logConverter.py path/to/communication/database path/to/binary/telemetry.bin telemetry.csv
```

In order to filter one or more `-f` arguments can be given. In the following example,
only telemetry of the `LOG` type is extracted:
```shell
logConverter.py path/to/communication/database path/to/binary/telemetry.bin -f LOG
```

For a description and an overview of all options, run
```shell
logConverter.py --help
```

## Dependencies

Requires the `ecom` module to be installed.
