#! /usr/bin/env python3
# -*- coding: utf-8 -*-
""" Allows to convert a binary telemetry log into the CSV format. """

import os
import csv
import sys

from typing import Iterator
from argparse import ArgumentParser, OPTIONAL
from contextlib import nullcontext
from collections import OrderedDict

try:
    from ecom.database import CommunicationDatabase
except ImportError:
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

    from ecom.database import CommunicationDatabase
from ecom.parser import TelemetryParser, TelecommandParser
from ecom.message import DependantTelecommandResponseType, Message
from ecom.checksum import ChecksumVerifier
from ecom.response import ResponseTelemetryParser


class ReplayResponseTelemetryParser(ResponseTelemetryParser):
    """ A telemetry parser that supports responses from telecommands """
    def __init__(self, database: CommunicationDatabase, verifier, recordedTelecommands):
        """
        Initialize a parser.

        :param database: The communication database.
        :param verifier: An optional verifier to use.
        :param recordedTelecommands: The readable open file to the recorded telecommands.
        """
        super().__init__(database, verifier=verifier)
        parser = TelecommandParser(database, verifier=verifier)
        self._telecommands = []
        for telecommand in parser.parse(recordedTelecommands.read(),
                                        errorHandler=lambda error: print(f'[Error] Telecommand parser error: {error}')):
            counter = telecommand.header['counter']
            if counter in self._telecommandCache:
                self._telecommands.append((counter, telecommand))
            else:
                self._putTelecommandInCache(telecommand, counter)

    def _putTelecommandInCache(self, telecommand, counter):
        """
        Put the telecommand into the internal cache, so it can be found during parsing.

        :param telecommand: The telecommand to put into the cache.
        :param counter: The counter value of the telecommand.
        """
        telecommandType = self._database.getTelecommand(telecommand.type)
        response = telecommandType.response
        if isinstance(response, DependantTelecommandResponseType):
            response = response.configureWith(telecommand.data[response.provider.name])
        self._telecommandCache[counter] = (telecommandType, response)

    def parse(self, *args, **kwargs) -> Iterator[Message]:
        for telemetry in super().parse(*args, **kwargs):
            if telemetry.type is self._TELEMETRY_RESPONSE:
                counter = telemetry.data['command number']
                for i, (telecommandCounter, telecommand) in enumerate(self._telecommands):
                    if counter == telecommandCounter:
                        del self._telecommands[i]
                        self._putTelecommandInCache(telecommand, counter)
                        break
            yield telemetry


def onParserError(error):
    """
    Handle a parser error while reading the binary log file.

    :param error: The parser error.
    """
    print(f'[Error] Parser error: {error}')


def getFilteredValues(telemetry, filters):
    """
    Filter values from the telemetry based on the provided filters.

    :param telemetry: The telemetry whose values should be filtered.
    :param filters: A dictionary of telemetry type names to data point paths which should be included.
    :return: The list of filtered values, one value or None for each filter.
    """
    if telemetry.type not in filters:
        return None
    values = []
    for telemetryType, typeFilters in filters.items():
        if telemetry.type is telemetryType:
            for filterItem in typeFilters:
                values.append(telemetry.data.get(filterItem))
        else:
            values.extend([None] * len(typeFilters))
    return values


def writeHeader(writer, filters):
    """
    Write CSV headers into the CSV writer from the given filters.

    :param writer: A CSV writer.
    :param filters: The active filters.
    """
    row = []
    for telemetryType, typeFilters in filters.items():
        typeName = telemetryType.name
        for filterItem in typeFilters:
            row.append(f'{typeName} - {filterItem}')
    writer.writerow(row)


def convertLogFile(parser, logFile, output, filters):
    """
    Convert a binary logfile into a CSV file.

    :param parser: The parser to use to parse the log file.
    :param logFile: The path ot the log file.
    :param output: The path to the output CSV file.
    :param filters: A dictionary of telemetry type names to data point paths which should be included.
    """
    csvWriter = csv.writer(output)
    writeHeader(csvWriter, filters)
    while True:
        buffer = logFile.read(2048)
        if not buffer:
            break
        for telemetry in parser.parse(buffer, errorHandler=onParserError):
            values = getFilteredValues(telemetry, filters)
            if values is not None:
                csvWriter.writerow(values)


def main():
    """
    Allow to convert a binary telemetry log file into CSV.

    :return: The exit code of the program.
    """
    parser = ArgumentParser(description='Convert a binary telemetry log into a CSV.')
    parser.add_argument('database', help='The communication database to use')
    parser.add_argument('logFile', help='The binary telemetry log to convert.')
    parser.add_argument('output', nargs=OPTIONAL,
                        help='Where to write the converted output to, defaults to stdout.')
    parser.add_argument('-t', '--telecommandLogFile',
                        help='A file with telecommands that were sent while the logfile was recorded.')
    parser.add_argument('-f', '--filter', action='append',
                        help='Filter in the form of "<MessageType>.<name>".')
    arguments = parser.parse_args()
    database = CommunicationDatabase(arguments.database)
    filters = OrderedDict()
    telemetryHeaderType = database.getTypeInfo('TelemetryMessageHeader')
    verifier = ChecksumVerifier(database) if 'checksum' in telemetryHeaderType.type else None
    if arguments.filter is None:
        for telemetryTypeInfo in database.telemetryTypes:
            filters[telemetryTypeInfo.id] = [typeInfo.name for typeInfo in telemetryTypeInfo.data]
    else:
        for filterItem in arguments.filter:
            filterParts = filterItem.split('.', 1)
            try:
                filterType = database.telemetryTypeEnum[filterParts[0]]
            except ValueError as error:
                print(f'[Error] Filter "{filterItem}" is invalid: {error}')
                return 1
            telemetryTypeInfo = next(filter(
                lambda telemetry: telemetry.id == filterType, database.telemetryTypes))
            availableFilterItems = [typeInfo.name for typeInfo in telemetryTypeInfo.data]
            if len(filterParts) == 2:
                if filterParts[1] not in availableFilterItems:
                    print(f'[Error] Filter "{filterItem}" is invalid: '
                          f'{filterParts[1]} is not an item in {filterParts[0]}')
                    return 1
                filterItems = filters.setdefault(filterType, [])
                if filterParts[1] not in filterItems:
                    filterItems.append(filterParts[1])
            else:
                filters[filterType] = availableFilterItems
    try:
        with open(arguments.logFile, 'rb') as logFile, nullcontext() if arguments.telecommandLogFile is None else \
                open(arguments.telecommandLogFile, 'rb') as telecommandLogFile:
            try:
                with nullcontext(sys.stdout) if arguments.output is None else \
                        open(arguments.output, 'w', encoding='utf-8', newline='') as outputFile:
                    if telecommandLogFile is None:
                        telemetryParser = TelemetryParser(database, verifier=verifier)
                    else:
                        telemetryParser = ReplayResponseTelemetryParser(database, verifier, telecommandLogFile)
                    convertLogFile(telemetryParser, logFile, outputFile, filters)
            except IOError as error:
                print(f'[Error] Failed to open output file: {error}')
                return 1
    except IOError as error:
        print(f'[Error] Failed to read from log file: {error}')
        return 1


if __name__ == '__main__':
    sys.exit(main())
