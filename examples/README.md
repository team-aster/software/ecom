# Examples

This directory contains some examples on how to work with the ECom system.

* [`database`](database) contains an example of a communication database. This is the files
  and folders that define how the communication between the devices looks like.
* [`logConverter`](logConverter) demonstrates how to use the `ecom` library to parse telemetry.
