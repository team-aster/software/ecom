#! /usr/bin/env python3
# -*- coding: utf-8 -*-
""" Provides a simple interface to view telemetry and send telecommands. """
import os
import sys
import json
import struct

from argparse import ArgumentParser
from threading import Thread

# noinspection PyPackageRequirements
from serial import Serial
# noinspection PyPackageRequirements
from serial.tools.list_ports import comports

try:
    from ecom.database import CommunicationDatabase, CommunicationDatabaseError
except ImportError:
    # If ecom is not installed, try finding it up the directory tree from the current file
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

    from ecom.database import CommunicationDatabase, CommunicationDatabaseError

from ecom.parser import TelemetryParser
from ecom.checksum import ChecksumVerifier
from ecom.response import ResponseTelemetryParser


class TelemetryReadThread(Thread):
    """ A thread responsible for handling data transfer between the experiment and the ground station. """
    def __init__(self, connection: Serial, parser: TelemetryParser):
        """
        Initialize the telemetry read thread.

        :param connection: The serial connection to the experiment.
        :param parser: A telemetry parser to parse incoming telemetry from the experiment.
        """
        super().__init__(daemon=True)
        self._connection = connection
        self._parser = parser
        self._isRunning = False

    def stop(self):
        self._isRunning = False

    def run(self):
        """ Handle telemetry from the experiment and send them to the main thread. """
        self._isRunning = True
        while self._isRunning and self._connection.isOpen():  # Check if we are still connected
            try:
                buffer = self._connection.read(self._connection.inWaiting() or 1)
            except (IOError, TypeError):
                if not self._isRunning:
                    return
                raise
            if buffer:
                for telemetry in self._parser.parse(buffer, errorHandler=self._onParserError,
                                                    ignoredBytesHandler=self._onIgnoredBytes):
                    print(f'{telemetry.type.name}: {telemetry.data}')

    @staticmethod
    def _onParserError(error):
        """
        Handle an error that occurred during parsing.

        :param error: The parsing error.
        """
        print(f'Parser error: {error}')

    def _onIgnoredBytes(self, _: bytes, ignoredBytes: int):
        """
        Handle bytes that were ignored during parsing.

        :param _: The current parsing buffer.
        :param ignoredBytes: The number of bytes that will get ignored.
        """
        print(f'Warning: Ignoring {ignoredBytes} bytes. Check that the serial connection is set up correctly '
              f'and the correct baudrate is used ({self._connection.baudrate})')


class Groundstation:
    def __init__(self, arguments):
        """
        Initialize the groundstation.

        :param arguments: Command line arguments.
        """
        super().__init__()
        databasePath = arguments.database
        if databasePath is None:
            databasePath = input('Enter the path to the communication database: ')
        self._database = CommunicationDatabase(databasePath)
        self._supportsSending = self._database.telecommandTypeEnum is not None
        verifier = ChecksumVerifier(self._database)
        if self._supportsSending:
            self._parser = ResponseTelemetryParser(self._database, verifier=verifier)
        else:
            self._parser = TelemetryParser(self._database, verifier=verifier)
        self._connection = Serial(baudrate=arguments.baudrate, timeout=1)
        try:
            self._connection.set_buffer_size(rx_size=640000)
        except AttributeError:
            pass  # Not all systems support setting the buffer size
        self._port = arguments.port

    def run(self):
        """
        Run the groundstation:

        * Determine the connection port to the experiment.
        * Connect to the port and start the telemetry thread.
        * Wait for input and send telecommands.
        """
        # Determine the connection port
        port = self._port
        if port is None:
            port = self._selectComPort()
        self._connection.setPort(port)
        self._connection.open()
        print(f'Connected to {port}')

        # Create and start the connection thread
        connectionThread = TelemetryReadThread(self._connection, self._parser)
        connectionThread.start()

        # Wait for input and execute the command
        try:
            while True:
                command = input()
                if command in ('q', 'Q', 'quit', 'exit'):
                    break
                if not self._supportsSending:
                    print("Error: The communication database doesn't define any telecommands")
                    continue
                try:
                    self._sendTelecommand(command)
                except ValueError as error:
                    print(f'Error: {error}')
                except IOError as error:
                    print(f'Failed to send command: {error}')
        except KeyboardInterrupt:
            print('Exiting...')

        # Cleanup
        connectionThread.stop()
        self._connection.close()
        connectionThread.join(2000)

    @staticmethod
    def _selectComPort():
        """
        Select a communication port from the list of all ports.
        If only one port is present, select it, otherwise show a dialog to select a port.

        :return: The communication port that was selected.
        """
        allPorts = comports()
        while not allPorts:
            input('No ports detected, please connect the target device and press enter')
            allPorts = comports()
        if len(allPorts) == 1:
            return allPorts[0].device
        for i, port in enumerate(allPorts):
            print(f'[{i}] {port.device}: {port.description}')
        while True:
            inputText = input(f'Select port [0-{len(allPorts) - 1}] ')
            try:
                return allPorts[int(inputText.strip())].device
            except (ValueError, IndexError):
                print('Not a valid choice')

    def _sendTelecommand(self, commandString):
        """
        Send the command to the experiment.

        :param commandString: The command as a string in the form of "<name>[ <json argument object>]".
        """
        commandName, argumentString, *_ = *commandString.split(maxsplit=1), '', ''
        try:
            telecommand = self._database.getTelecommandByName(commandName)
        except ValueError:
            raise ValueError(f'Unknown command {commandString!r}')
        argumentString = argumentString.strip()
        if argumentString:
            try:
                arguments = json.loads(argumentString)
            except json.JSONDecodeError as error:
                raise ValueError(f'Arguments are not valid json: {error}')
        else:
            arguments = {}
        try:
            # noinspection PyUnresolvedReferences
            buffer = self._parser.serialize(telecommand, **arguments)
        except (ValueError, TypeError, struct.error) as error:
            raise ValueError(f'Invalid arguments: {error}')
        self._connection.write(buffer)


def main():
    """ Parse the command line arguments and show the debugging interface. """
    parser = ArgumentParser(description='Debugging interface for ECom.')
    parser.add_argument('-p', '--port', default=None,
                        help='The serial port that the ECom project is connected to.')
    parser.add_argument('-b', '--baudrate', help='The baudrate to use for the serial connection.',
                        type=int, default=19200)
    parser.add_argument('-d', '--database', help='The communication database.')
    arguments = parser.parse_args()
    try:
        groundstation = Groundstation(arguments)
    except CommunicationDatabaseError as error:
        print(f'[Error] Failed to load communication database: {error}')
        return 1
    groundstation.run()


if __name__ == '__main__':
    sys.exit(main())
