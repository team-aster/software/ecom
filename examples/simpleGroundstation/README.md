# Simple groundstation

This example demonstrates a very simple terminal based groundstation.
It is able to print received messages and allows to send telecommands.


## Usage
If just called with a path to a communication database, it will automatically connect
to a serial port or ask the user, if multiple ports are present. If no baudrate is provided,
a baudrate of `19200` will be used.
```shell
groundstation.py -d ../database
```

The port and baudrate can be configured via parameters:
```shell
groundstation.py -d ../database --baudrate 115200 --port COM5
```

For a description and an overview of all options, run
```shell
groundstation.py --help
```

### Receive telemetry
The groundstation will automatically start printing all received messages when connected.

### Sending telecommands
To send a telecommand, type the name of the telecommand into the terminal when the groundstation is running.
If the telecommand has arguments, they can be given as a json object after a space:
```
MY_TELECOMMAND {"argument1": false}
```

### Exiting
The groundstation can be closed by either pressing `Ctrl` + `C` or `q`, `Q`, `quit`, `exit` followed by an `Enter`.

## Dependencies
Requires the `ecom` module to be installed and all modules from the [`requirements.txt`](requirements.txt) file.
They can be installed by executing the following command in the directory containing this README:
```shell
pip install -r requirements.txt
```
