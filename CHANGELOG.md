# ECom changelog

## 1.1.1 (2023-02-12)

A minor release with bugfixes around fixed size arrays in configurations.

### Fixes

* Python
  * Fixed parsing of with a fixed size
* C
  * Fixed generated `handleCommandSetConfig` handler for nontrivial types (like arrays).

## 1.1.0 (2023-02-09)

The Python library is now also capable of serializing telemetry and deserializing telecommands,
which means that Python to Python communication is now possible.
Additionally, telecommands can now have dynamically sized elements.

This release contains **API breaking changes**.

### Additions

* Python
  * Added common base class `ecom.message.MessageType` for `TelemetryType` and `TelecommandType`.
  * Added common base class `ecom.message.MessageDatapointType`
    for `TelemetryDatapointType` and `TelecommandDatapointType`.
  * Added `CommunicationDatabase.getTelecommandByName` to find a telecommand type by name.
  * Added `CommunicationDatabase.getTelecommand` to find a telecommand type its id.
  * Added `CommunicationDatabase.getTelemetryByName` to find a telemetry type by name.
  * Added `CommunicationDatabase.getTelemetry` to find a telemetry type by its id.
  * Added `CommunicationDatabase.parseKnownTypeInfo` which allows to parse a `TypeInfo` for a known type.
    This allows to parse array type specification (e.g. `'char[5]'`)
  * Added `telecommandTypeEnum`, `telemetryTypeEnum` and `configurationEnum` members to `CommunicationDatabase`
    to access the enum type for telecommand, telemetry and config ids.
  * Added `CommunicationDatabase.replaceType` to replace a Python type in the database.
    This can be used to define a custom class for a shared datatype that should be used
    instead of the automatically generated class.
  * Added `CommunicationDatabase.registerChangeListener` to register a listener which gets called
    when the database is mutated (e.g. via `replaceType`).
  * Added `__contains__` method to `StructType` classes which allows to check
    if a struct type has a child with a given name (e.g. `'child' in struct`).
  * Added `offsetOf` method to `StructType` classes to calculate the offset in bytes
    of a child by name from the start of the struct.
  * Added `getMaxNumericValue` to `TypeInfo` to get a types maximum numeric value.
  * Added `getMinNumericValue` to `TypeInfo` to get a types minimum numeric value.
  * Added `TypeInfo.copyWithType` to create a copy of the TypeInfo with a different Python type.
  * Added `ecom.serializer.TelemetrySerializer`, which can serialize telemetry.
  * Added `ecom.parser.TelecommandParser`, which can parse telecommands.
  * Added `ecom.message.Telemetry` subclass of `Message`.
  * Added `ecom.message.Telecommand` subclass of `Message`.
  * Added `header` member to `Message` which allows access to the data received in a message header.
  * Added `ecom.response.FixedSizeResponseTelemetrySerializer` for serializing telecommand responses
    where the response data is put into a value filed that has the same size for all response messages.
  * Added `ecom.response.VariableSizedResponseTelemetrySerializer` for serializing telecommand responses
    where the response data is sent in a dynamically sized member alongside its size.
  * Added `ecom.database.Constant` to hold information about a constant in the communication database.
  * All generated Enum, struct and array classes can now be compared (using `==`) to check if they are compatible.
    For Enums, this means that both enums must have the same members with the same values and names.
    For structs, this means that both structs must have the same children with the same names and types.
    For arrays, this means that both arrays must have the same size (or dynamic size accessor)
    and the element type must match.
  * Generated Enum values now compare true to enum values with the same name and value of a compatible Enum class.
  * Communication database instances can now be compared to other communication databases
    to check if they contain the same communication definition.
  * Added `ecom.datatypes.structDataclass` decorator that can be used to generate a struct class
    where the members of the struct can be accessed like in a dataclass.
  * Added `ecom.datatypes.loadTypedValue` function to parse a value into a given Python type.
  * Added `buffer` member to `ParserErrors` which contains the parser buffer at the point the error was generated.
  * Added `message.iterateRequiredDatapoints` which can be used to iterate over just the required telecommand
    datapoints and ignore those datapoints that can be inferred.
  * `Parser.parse` got a new optional `ignoredBytesHandler` argument.
    This callable will be called when the parser is about to ignore some bytes.
  * `TelecommandSerializer` got a new property `nextTelecommandCounter`,
    which reports the next telecommand counter that will be used for the next telecommand.
* C
  * Added support for receiving dynamically sized telecommands.
* Python / C
  * Added automatically generated `MAX_CONFIG_VALUE_SIZE` constant which holds
    the maximum size in bytes of any configuration value.
  * Added checks that a dynamically sized message component doesn't exceed a maximum size.
    This is `512` by default and can be configured on the Python side by providing `maxDynamicMemberSize` to a parser.

### Changes

* Python
  * **Breaking changes**
    * `TelemetryResponseType`
      - Moved from `ecom.parser` to `ecom.message`
      - Renamed to `TelemetryType`.
    * `TelemetryResponseDataPoint`
      - Moved from `ecom.parser` to `ecom.message`.
      - Renamed to `TelemetryDatapointType`.
    * `DependantTelecommandResponse`
      - Moved from `ecom.parser` to `ecom.message`.
      - Renamed to `DependantTelecommandResponseType`.
    * `TelemetryResponse`
      - Moved from `ecom.parser` to `ecom.message`.
      - Renamed to `Message`.
    * `TelecommandArgument`
      - Moved from `ecom.serializer` to `ecom.message`.
      - Renamed to `TelecommandDatapointType`.
      - Renamed `typeInfo` member to `type`.
    * `DependantTelecommandArgument`
      - Moved from `ecom.serializer` to `ecom.message`.
      - Renamed to `DependantTelecommandDatapointType`.
    * `TelecommandResponse`
      - Moved from `ecom.serializer` to `ecom.message`.
      - Renamed to `TelecommandResponseType`.
    * `Telecommand`:
      - Moved from `ecom.serializer` to `ecom.message`.
      - Renamed to `TelecommandType`.
      - `id` member is now an enum instead of an int.
      - Removed `name` member, it is replaced by `id.name`.
      - Renamed `arguments` member to `data`.
    * `CommunicationDatabase`
      - Renamed `telecommands` member to `telecommandTypes`.
      - `constants` no longer returns a tuple, a `Constant` is now returned.

        `constants['name'][0]` now becomes `constants['name'].value`.
        `constants['name'][1]` now becomes `constants['name'].description`.
        `constants['name'][2]` now becomes `constants['name'].type`.
    * Renamed `ConfigurationValueArgument` to `ConfigurationValueDatapoint`.
    * Renamed `ConfigurationValueResponse` to `ConfigurationValueResponseType`.
    * Parsers and serializers will no longer automatically add/verify a CRC16 checksum for each message.
      A `MessageVerifier` (for CRC16, specifically a `ChecksumVerifier`).
      has to be provided when creating the parser/serializer.
    * Removed `telemetryTypeEnum` member on the parser, it is now accessible via a `CommunicationDatabase` instance.
    * Moved `ResponseTelemetryParser` from `ecom.responseParser` to `ecom.response`.
    * Renamed `UnknownArgumentError` to `UnknownDatapointError`
      and `UnknownArgumentError.argumentName` to `UnknownDatapointError.datapointName`.
  * Other changes.
    * The verification of a message can now be customized by providing a `MessageVerifier` to serializers and parsers.
    * `TypeInfo.getBaseType` now returns a `BaseType` enum value instead of a string.
    * `TypeInfo.getFormat` now accepts an optional `values` dictionary containing values which can get used
      when resolving a dynamic size.
    * `TelecommandSerializer` no longer requires a `counter` field in the telecommand header. 
      If a counter field is present, the maximum counter value is automatically deduced from its type.
      The name of the field can now be changed by providing a different `counterMemberName` to the serializer.
    * Configurations are now loaded later than shared types,
      so they can now use shared types defined in the database.
    * Serializers can now automatically infer values for size members of other array members.
* C
  * **Breaking changes**
    * Renamed `generated/telecommands.h` to `generated/receiver.h`
    * Removed `handleTelecommand` function, it is now replaced by `parseMessage`
      synonymous to the code generated with the `--forBase` flag.
      This means that telecommand handler are no longer freestanding functions,
      but must be passed to `parseMessage` in a `MessageHandler` object.
  * Other
    * Fixed handling of parser errors. In some cases the parser code would just return without discarding at least
      one byte of the invalid message, causing it to try and attempt parsing the same message again and again.
* Python / C
  * Added support for sync bytes with repeating patterns
  * **Breaking changes**
    * The automatically generated `Telecommand` type has been renamed to `TelecommandType`.
* Communication database:
  * **Breaking changes**
    * A `GroundStationMessage` shared datatype is no longer required, instead a `TelecommandMessageHeader`
      must be provided. It represents a header that is sent at the beginning of each telecommand message
      and must have a `type` member of the type `TelecommandType`.
    * Removed `_t` suffix from all base types:
      - `int8_t` was renamed to `int8`.
      - `uint8_t` was renamed to `uint8`.
      - `int16_t` was renamed to `int16`.
      - `uint16_t` was renamed to `uint16`.
      - `int32_t` was renamed to `int32`.
      - `uint32_t` was renamed to `uint32`.
      - `int64_t` was renamed to `int64`.
      - `uint64_t` was renamed to `uint64`.

## 1.0.0 (2022-11-29)

Initial release

### Added
* Telecommands: Messages from the base to a secondary device.
* Telemetry: Messages from the secondary device to the base device.
* Units: A type representing a physical unit, creates a specific type in C allowing to leverage C's type system
  to prevent errors with units.
* Constants: Constant values that are shared with both sides of the communication.
* Shared datatypes: A way of defining complex data types that are shared with both sides of the communication.
* Configuration: A special key value mapping that can be queried and updated via telecommands.
* Python module to read communication database, send telecommands and parse telemetry
* Python script to generate C code capable of parsing and serializing both telemetry and telecommands.
* This release supports Python to C (embedded) communication, as well as C to C communication.
