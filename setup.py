import os

from glob import glob
from setuptools import setup

from ecom import __version__

setup(
    name='ecom',
    version=__version__,
    author='Aster',
    author_email='aster.rexus@gmail.com',
    description='Efficient communication library',
    license='MIT',
    url='https://gitlab.com/team-aster/software/ecom',
    packages=['ecom'],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
    python_requires='>=3.7.0',
    extras_require={
        'docs': [
            'sphinx~=5.1',
            'sphinx_rtd_theme~=1.1',
            'myst-parser~=0.18.1',
            'sphinx-autoapi~=2.0.1',
            'docutils>=0.17',
            'pygments~=2.14.0',
        ],
    },
    scripts=[
        os.path.join('scripts', 'ecomUpdate.py'),
    ],
    data_files=[
        ('ecom/templates', glob(os.path.join('scripts', 'templates', '*'))),
    ],
    bdist_wheel={
        'universal': True
    },
)
