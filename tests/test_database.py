import os
import dataclasses

from shutil import copytree
from typing import Iterable, TypeVar, Callable
from tempfile import TemporaryDirectory

from ecom.parser import TelemetryParser, TelecommandParser
from ecom.message import Telecommand
from ecom.checksum import ChecksumVerifier
from ecom.database import CommunicationDatabase, UnknownTypeError, CommunicationDatabaseError
from ecom.datatypes import EnumType, StructType, structDataclass, structField
from ecom.serializer import TelecommandSerializer, TelemetrySerializer

from tests import TEST_DATA_DIR, EComTestCase

T = TypeVar('T')


class TestDatabase(EComTestCase):
    """ Test the database. """

    def testLoadsTheExample(self):
        """ Test that the database loads the example correctly. """
        database = CommunicationDatabase(TEST_DATA_DIR)
        self.assertEqual(len(database.constants), 9)
        self.assertEqual(len(database.telecommandTypes), 24)
        self.assertEqual(len(database.telemetryTypes), 11)
        self.assertEqual(len(database.dataTypes), 15)
        self.assertEqual(len(database.units), 11)
        self.assertEqual(len(database.configurations), 37)
        boolConfiguration = self._findFirst(
            database.configurations, lambda configuration: configuration.name == 'use motor speeds for controller')
        self.assertFalse(boolConfiguration.defaultValue)
        self.assertNotIn(
            'MAX_TELEMETRY_DATA_SIZE', database.constants,
            'Expected not to have a maximum telemetry data size in a database with variable sized telemetry')
        self.assertEqual(database.constants['MAX_TELECOMMAND_DATA_SIZE'].value, 65)
        self.assertEqual(database.constants['MAX_TELECOMMAND_RESPONSE_SIZE'].value, 64)
        self.assertEqual(database.constants['MAX_CONFIG_VALUE_SIZE'].value, 64)
        self.assertEqual(database.constants['NUM_CONFIGURATIONS'].value, 37)

    @staticmethod
    def _findFirst(sequence: Iterable[T], filterCallback: Callable[[T], bool]) -> T:
        for value in sequence:
            if filterCallback(value):
                return value
        raise ValueError('No value in the sequence satisfied the filter')

    def testRejectsEmptyDirectory(self):
        """ Test that the database rejects loading an empty directory. """
        with self.assertRaisesRegex(CommunicationDatabaseError, 'sharedDataTypes.json',
                                    msg='The database should not allow a directory without shared types'):
            with TemporaryDirectory() as tempDir:
                CommunicationDatabase(tempDir)

    def testRepr(self):
        """ Test that the database correctly represents itself. """
        self.assertIn(repr(TEST_DATA_DIR), repr(CommunicationDatabase(TEST_DATA_DIR)),
                      'Expected the communication database to represent itself with the path to the database')

    def testDatabaseComparison(self):
        """ Test that database instances can be compared. """
        database1 = CommunicationDatabase(TEST_DATA_DIR)
        database2 = CommunicationDatabase(TEST_DATA_DIR)
        self.assertEqual(database1, database2, msg='Expected two CommunicationDatabases loaded '
                                                   'from the same directory to be equal')
        with TemporaryDirectory() as tempDir:
            tempDatabaseDir = os.path.join(tempDir, 'database')
            copytree(TEST_DATA_DIR, tempDatabaseDir)
            with open(os.path.join(tempDatabaseDir, 'sharedConstants.csv'),
                      mode='a', encoding='utf-8') as constantsFile:
                # Add additional constant
                constantsFile.write('TEST_CONSTANT,12345,uint32,'
                                    'A test constant which is a difference to the original database.\n')
            database3 = CommunicationDatabase(tempDatabaseDir)

        self.assertNotEqual(database1, database3, msg='Expected a database with an additional constant to be not equal '
                                                      'to another database without that constant')

    def testSupportsReplacingPythonStructTypes(self):
        """ Test that the communication database supports replacing struct types. """
        database = CommunicationDatabase(TEST_DATA_DIR)
        originalDatabase = CommunicationDatabase(TEST_DATA_DIR)
        verifier = ChecksumVerifier(database)
        serializer = TelemetrySerializer(database, verifier=verifier)
        parser = TelemetryParser(database, verifier=verifier)
        dataTelemetry = database.getTelemetryByName('DATA3')

        @structDataclass(database, replaceType=True)
        class Quaternion(StructType):
            x: float
            y: float
            z: float
            w: float

        self.assertIs(database.getTypeInfo('Quaternion').type, Quaternion,
                      'Expected the communication database to have replaced the type')
        for datapoint in database.getTelemetryByName('DATA3').data:
            if datapoint.name == 'orientation':
                break
        else:
            self.fail('Expected an orientation datapoint in DATA3')
        self.assertIs(datapoint.type.type, Quaternion,
                      'Expected the communication database to have replaced the type also for telemetry datapoints')
        payload = serializer.serialize(
            dataTelemetry, time=1234, orientation=Quaternion({'x': 1, 'y': 2, 'z': 3, 'w': 4}),
            **{'battery level': 1.234})
        parsedCommand = self._assertParsesOne(
            parser, payload, 'Expected the parser to parse a message with a replaced type')  # type: Telecommand
        self.assertEqual(parsedCommand.data['orientation'], Quaternion({'x': 1, 'y': 2, 'z': 3, 'w': 4}),
                         'Expected the parsed message to contain an instance of the replaced class with equal values')
        self.assertEqual(parsedCommand.data['orientation'].x, 1,
                         'Expected to be able to access the values using the member properties')
        self.assertEqual(database, originalDatabase,
                         msg='Expected the database still be equal to the original database')

        with self.assertRaises(UnknownTypeError, msg="Expected the structDataclass decorator to reject replacing a "
                                                     "struct that doesn't exist in the database"):
            # noinspection PyUnusedLocal
            @structDataclass(database, replaceType=True)
            class WrongName(StructType):
                x: float
                y: float
                z: float
                w: float

        @structDataclass(database, replaceType='Quaternion')
        class WrongName(StructType):
            x: float
            y: float
            z: float
            w: float
        self.assertIs(database.getTypeInfo('Quaternion').type, WrongName,
                      'Expected the structDataclass decorator to use the given name when replacing a struct')
        payload = serializer.serialize(
            dataTelemetry, time=1234, orientation=WrongName({'x': 1, 'y': 2, 'z': 3, 'w': 4}),
            **{'battery level': 1.234})
        parsedCommand = self._assertParsesOne(
            parser, payload, 'Expected the parser to parse a message with a replaced type')  # type: Telecommand
        self.assertEqual(parsedCommand.data['orientation'], WrongName({'x': 1, 'y': 2, 'z': 3, 'w': 4}),
                         'Expected the parsed message to contain an instance of the replaced class with equal values')
        self.assertEqual(database, originalDatabase,
                         msg='Expected the database still be equal to the original database')

        @structDataclass(database, replaceType='Quaternion')
        class TooManyStructChildren(Quaternion):
            a: float = dataclasses.field(default=5.0)

        structInstance = TooManyStructChildren({'x': 1, 'y': 2, 'z': 3, 'w': 4})
        self.assertDictEqual(dict(structInstance), {'x': 1, 'y': 2, 'z': 3, 'w': 4},
                             'Expected a non structFiled to be ignored when converting a structDataclass into a dict')
        self.assertEqual(structInstance.a, 5.0,
                         'Expected an ignored non-structField to be have like a normal dataclass field.')
        self.assertEqual(database, originalDatabase,
                         msg='Expected the database still be equal to the original database')

        with self.assertRaisesRegex(TypeError, 'Missing child.*\'w\'',
                                    msg='Expected a replacing structDataclass decorator to reject a class '
                                        'that does not contain all fields of the replaced struct'):
            # noinspection PyUnusedLocal
            @structDataclass(database, replaceType='Quaternion')
            class MissingStructItem(StructType):
                x: float
                y: float
                z: float

        with self.assertRaisesRegex(
                TypeError, 'Missing child.*\'w\'',
                msg='Expected a replacing structDataclass decorator to reject a class '
                    'where one member has a different name than the corresponding member of the original struct'):
            # noinspection PyUnusedLocal
            @structDataclass(database, replaceType='Quaternion')
            class TypoStructItem(StructType):
                x: float
                y: float
                z: float
                wOther: float

        @structDataclass(database, replaceType='Quaternion')
        class TypoStructItem(StructType):
            x: float
            y: float
            z: float
            wOther: float = structField(name='w')

        structInstance = TypoStructItem({'x': 1, 'y': 2, 'z': 3, 'w': 4})
        self.assertDictEqual(dict(structInstance), {'x': 1, 'y': 2, 'z': 3, 'w': 4},
                             'When the field name differs from the struct member name, '
                             'expected to use the struct member name when converting the struct to a dict')
        self.assertEqual(structInstance.wOther, 4, 'Expected to be able to access the underlying struct member '
                                                   'from a field with a different name')
        structInstance.wOther = 5
        self.assertDictEqual(dict(structInstance), {'x': 1, 'y': 2, 'z': 3, 'w': 5},
                             'Expected to be able to set the underlying struct member '
                             'from a field with a different name')
        self.assertEqual(database, originalDatabase,
                         msg='Expected the database still be equal to the original database')

        with self.assertRaisesRegex(
                TypeError, '\'w\'.*incompatible type.*bool.*!=.*float',
                msg='Expected a replacing structDataclass decorator to reject a class '
                    'where one member has a different type than the corresponding member of the original struct'):
            # noinspection PyUnusedLocal
            @structDataclass(database, replaceType='Quaternion')
            class WrongItemType(StructType):
                x: float
                y: float
                z: float
                w: bool

        @structDataclass(database, replaceType=True)
        class Vec3F(StructType):
            roll: float
            pitch: float
            yaw: float

        self.assertIs(database.dataTypes['Vec3FWithTime'].type['data'].type, Vec3F,
                      'Expected the database to have replaced the struct in child info of existing structs as well')
        for datapoint in database.getTelemetryByName('DATA_ROTATION_SPEEDS').data:
            if datapoint.name == 'rotation speeds':
                break
        else:
            self.fail('Expected a "rotation speeds" datapoint in DATA_ROTATION_SPEEDS')
        self.assertIs(datapoint.type.type.getElementTypeInfo().type['data'].type, Vec3F,
                      'Expected the database to have replaced indirectly changed types as well')
        self.assertEqual(database, originalDatabase,
                         msg='Expected the database still be equal to the original database')

    def testSupportsReplacingPythonEnumTypes(self):
        """ Test that the communication database supports replacing enum types. """
        class SystemState(EnumType):
            STATE_IDLE = 0
            STATE_PRE_LAUNCH = 1
            STATE_LAUNCH = 2
            STATE_EXPERIMENT = 3
            STATE_RECOVERY = 4
            STATE_PAYLOAD_OFF = 5
            STATE_TESTING = 6

        database = CommunicationDatabase(TEST_DATA_DIR)
        originalDatabase = CommunicationDatabase(TEST_DATA_DIR)
        verifier = ChecksumVerifier(database)
        serializer = TelecommandSerializer(database, verifier=verifier)
        parser = TelecommandParser(database, verifier=verifier)
        systemStateCommand = database.getTelecommandByName('SET_CURRENT_STATE')

        database.replaceType(SystemState)
        self.assertIs(database.getTypeInfo('SystemState').type, SystemState,
                      'Expected the communication database to have replaced the enum type')
        for datapoint in database.getTelemetryByName('DATA2').data:
            if datapoint.name == 'state':
                break
        else:
            self.fail('Expected an state datapoint in DATA2')
        self.assertIs(datapoint.type.type, SystemState,
                      'Expected the communication database to have replaced the type also for telemetry datapoints')
        self.assertIs(database.getTelecommandByName('GET_CURRENT_STATE').response.typeInfo.type, SystemState,
                      'Expected the communication database to have replaced the type also for telecommand responses')
        for datapoint in database.getTelecommandByName('SET_CURRENT_STATE').data:
            if datapoint.name == 'state':
                break
        else:
            self.fail('Expected an state datapoint in SET_CURRENT_STATE')
        self.assertIs(datapoint.type.type, SystemState,
                      'Expected the communication database to have replaced the type also for telecommand datapoints')
        payload = serializer.serialize(systemStateCommand, state=SystemState.STATE_LAUNCH)
        parsedCommand = self._assertParsesOne(
            parser, payload, 'Expected the parser to parse a message with a replaced type')  # type: Telecommand
        self.assertIs(parsedCommand.data['state'], SystemState.STATE_LAUNCH,
                      'Expected the parser to parse the data using the replaced enum type')
        self.assertEqual(database, originalDatabase,
                         msg='Expected the database still be equal to the original database')

        class WrongName(EnumType):
            STATE_IDLE = 0
            STATE_PRE_LAUNCH = 1
            STATE_LAUNCH = 2
            STATE_EXPERIMENT = 3
            STATE_RECOVERY = 4
            STATE_PAYLOAD_OFF = 5
            STATE_TESTING = 6

        with self.assertRaises(UnknownTypeError,
                               msg='Expected replaceType to reject a type that is not registered in the database'):
            database.replaceType(WrongName)
        database.replaceType(WrongName, name='SystemState')
        self.assertIs(database.getTypeInfo('SystemState').type, WrongName,
                      'Expected replaceType to replace the type with the explicit given name')
        payload = serializer.serialize(systemStateCommand, state=SystemState.STATE_LAUNCH)
        parsedCommand = self._assertParsesOne(
            parser, payload, 'Expected the parser to parse a message with a replaced type')  # type: Telecommand
        self.assertIs(parsedCommand.data['state'], WrongName.STATE_LAUNCH,
                      'Expected the parser to parse the data using the replaced enum type')
        self.assertEqual(database, originalDatabase,
                         msg='Expected the database still be equal to the original database')

        class TooManyEnumItems(EnumType):
            STATE_IDLE = 0
            STATE_PRE_LAUNCH = 1
            STATE_LAUNCH = 2
            STATE_EXPERIMENT = 3
            STATE_RECOVERY = 4
            STATE_PAYLOAD_OFF = 5
            STATE_TESTING = 6
            STATE_ADDITIONAL = 7

        with self.assertRaisesRegex(TypeError, 'does not match the existing type',
                                    msg='Expected replaceType to reject an enum with '
                                        'an additional value compared to the original enum'):
            database.replaceType(TooManyEnumItems, name='SystemState')

        class MissingEnumItem(EnumType):
            STATE_IDLE = 0
            STATE_PRE_LAUNCH = 1
            STATE_LAUNCH = 2
            STATE_EXPERIMENT = 3
            STATE_RECOVERY = 4
            STATE_PAYLOAD_OFF = 5

        with self.assertRaisesRegex(TypeError, 'does not match the existing type',
                                    msg='Expected replaceType to reject an enum with a '
                                        'missing value compared to the original enum'):
            database.replaceType(MissingEnumItem, name='SystemState')

        class TypoEnumItem(EnumType):
            STATE_IDLE = 0
            STATE_PRE_LAUNCH = 1
            STATE_LAUNCH = 2
            STATE_EXPERIMENT = 3
            STATE_RECOVERY = 4
            STATE_PAYLOAD_OFF = 5
            STATE_TESTING_ = 6

        with self.assertRaisesRegex(TypeError, 'does not match the existing type',
                                    msg='Expected replaceType to reject an enum where one member '
                                        'has a different name compared to the original enum'):
            database.replaceType(TypoEnumItem, name='SystemState')

        class WrongItemValue(EnumType):
            STATE_IDLE = 0
            STATE_PRE_LAUNCH = 1
            STATE_LAUNCH = 2
            STATE_EXPERIMENT = 3
            STATE_RECOVERY = 4
            STATE_PAYLOAD_OFF = 5
            STATE_TESTING = 7

        with self.assertRaisesRegex(TypeError, 'does not match the existing type',
                                    msg='Expected replaceType to reject an enum where one member '
                                        'has a different value compared to the original enum'):
            database.replaceType(WrongItemValue, name='SystemState')

    def testChangeListenersAreCalled(self):
        """ Verify that change listeners are called when a change occurs in the database. """
        database = CommunicationDatabase(TEST_DATA_DIR)
        listenerCalls = 0

        def listener():
            nonlocal listenerCalls
            listenerCalls += 1
        database.registerChangeListener(listener)

        # noinspection PyUnusedLocal
        @structDataclass(database, replaceType=True)
        class Quaternion(StructType):
            x: float
            y: float
            z: float
            w: float

        self.assertEqual(listenerCalls, 1,
                         'Expected the change listener to be invoked exactly one times for one change.')
