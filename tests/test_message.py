from ecom.message import iterateRequiredDatapoints
from ecom.database import CommunicationDatabase

from tests import TEST_DATA_DIR, EComTestCase


class TestDatatypes(EComTestCase):
    """ Test the message functionalities. """

    def testIterateRequiredDatapoints(self):
        """ Test that iterating over the required telecommands works. """
        database = CommunicationDatabase(TEST_DATA_DIR)
        setFlightCommand = database.getTelecommandByName('SET_FLIGHT')
        self.assertListEqual(setFlightCommand.data, list(iterateRequiredDatapoints(setFlightCommand)),
                             msg='Expected iterateRequiredDatapoints to return all datapoints if none are optional')
        sendCommandCommand = database.getTelecommandByName('SEND_COMMAND')
        self.assertListEqual([sendCommandCommand.data[1]], list(iterateRequiredDatapoints(sendCommandCommand)),
                             msg='Expected iterateRequiredDatapoints to only return required datapoints')
