""" Unit tests for the ecom package. """
import os
from abc import ABC
from enum import Enum
from typing import TypeVar
from unittest import TestCase

from ecom.parser import ParserError, Parser

T = TypeVar('T')

TEST_DATA_DIR = os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..', 'examples', 'database'))

# noinspection SpellCheckingInspection
TEST_DATA_TELEMETRIES = {
    'DATA1': ('<Ifffffffbbffffff', lambda database: {
        'time': 123455, 'processor temperature': 32.5, 'battery temperature': 38.6,
        'Pdu Pcb temperature': -10.34, 'motor controller temperature': 100.44,
        'motor bracket temperature': 9999.34, 'retention wall temperature': 10.05,
        'recovery wall temperature': 23.45, 'primary imu temperature': 25,
        'secondary imu temperature': -2,
        'motor current': {'roll': 321.54, 'pitch': -32.546, 'yaw': -13.56},
        'secondary imu rotation': {'roll': 4.56, 'pitch': 2.34, 'yaw': -3.4},
    }),
    'DATA2': ('<IfffffffffffffB', lambda database: {
        'time': 123455, 'primary x acceleration': 5476.86, 'primary y acceleration': 4.4545,
        'primary z acceleration': 56.876, 'primary x magnetic force': 0.123414,
        'primary y magnetic force': 0.000214, 'primary z magnetic force': 0.0,
        'secondary x acceleration': 54.836, 'secondary y acceleration': -4.75,
        'secondary z acceleration': 5.6, 'secondary x magnetic force': 0.04,
        'secondary y magnetic force': -24.56, 'secondary z magnetic force': 340.0,
        'pressure': 1.08765,
        'state': database.dataTypes['SystemState'].type['STATE_TESTING'],
    }),
    'DATA3': ('<Iddddf', lambda database: {
        'time': 123455,
        'orientation': {'x': -23.4325, 'y': 1323.5235, 'z': 434.5463, 'w': 0.1415},
        'battery level': 54.556,
    }),
    'DATA_MOTOR_SPEEDS': ('<BIfffIfffIfff', lambda database: {
        'motor speed count': 3,
        'motor speeds': [
            {'time': 0, 'data': {'roll': 1.23, 'pitch': 212.34, 'yaw': -331.4}},
            {'time': 1, 'data': {'roll': -24.342, 'pitch': 0.001, 'yaw': 213.41313}},
            {'time': 2, 'data': {'roll': 212.423, 'pitch': 212.34, 'yaw': 3131.21}},
        ],
    }),
    'DATA_ROTATION_SPEEDS': ('<BIfffIfff', lambda database: {
        'rotation speed count': 2,
        'rotation speeds': [
            {'time': 245, 'data': {'roll': 75.56, 'pitch': -2.565, 'yaw': 2323.64}},
            {'time': 453, 'data': {'roll': -2342.34, 'pitch': 3.5436, 'yaw': -46.64457}},
        ],
    }),
    'DATA_SET_POINTS': ('<BIiiiIiii', lambda database: {
        'set points count': 2,
        'set points': [
            {'time': 1234, 'data': {'roll': 945342, 'pitch': -534536, 'yaw': 855954593}},
            {'time': 5678, 'data': {'roll': 654564, 'pitch': 423443, 'yaw': -1}},
        ],
    }),
    'DATA_RECOVERY': ('<IIdddB', lambda database: {
        'time': 123455, 'gps time': 456789, 'longitude': 45.67856, 'latitude': 6.778843,
        'height': 5323.353,
        'recovery state': database.dataTypes['RecoveryState'].type['RECOVERY_LANDED'],
    }),
}
# noinspection SpellCheckingInspection
TEST_DATA_TELECOMMANDS = {
    'NEW_LOG_FILE': ('<BB', lambda database, counter: {
        'counter': counter,
        'type': database.telecommandTypeEnum['NEW_LOG_FILE'],
    }),
    'RESET_CHIP': ('<BBI', lambda database, counter: {
        'counter': counter,
        'type': database.telecommandTypeEnum['RESET_CHIP'],
        'verification code': 123456,
    }),
    'GET_LAST_STATE': ('<BBI', lambda database, counter: {
        'counter': counter,
        'type': database.telecommandTypeEnum['GET_LAST_STATE'],
    }),
    'SET_CONFIG': ('<BBBd', lambda database, counter: {
        'counter': counter,
        'type': database.telecommandTypeEnum['SET_CONFIG'],
        'config': database.configurationEnum['parachute trigger altitude'],
        'value': 2.2222,
    }),
    'GET_CONFIG': ('<BBBdddddddd', lambda database, counter: {
        'counter': counter,
        'type': database.telecommandTypeEnum['GET_CONFIG'],
        'config': database.configurationEnum['target orientations'],
    }),
}


class EComTestCase(TestCase, ABC):
    def _assertAlmostEqual(self, first, second, delta=None, msg=None):
        """
        Assert that two values are almost equal. Supports nested types.

        :param first: The first value.
        :param second: The second value.
        :param delta: The maximal difference between the two values.
        :param msg: An optional message to display if the two vales differ sufficiently.
        """
        if isinstance(first, list) and isinstance(second, list):
            self.assertEqual(len(first), len(second), msg=msg)
            for i, element in enumerate(first):
                self._assertAlmostEqual(element, second[i], delta=delta, msg=msg)
        elif isinstance(first, dict) and isinstance(second, dict):
            self.assertEqual(len(first), len(second), msg=msg)
            for name, element in first.items():
                self._assertAlmostEqual(element, second[name], delta=delta, msg=msg)
        else:
            self.assertAlmostEqual(first, second, delta=delta, msg=msg)

    def _assertParserError(self, parser, data, description):
        """
        Assert that the parser generates one parser error.

        :param parser: The parser to use for parsing.
        :param data: The data to hand to the parser.
        :param description: A description of the message in the data.
        """
        errors = []
        for telemetry in parser.parse(data, errorHandler=errors.append):
            self.fail(f'Expected the parser not to read a telemetry from a message '
                      f'with {description}, got {telemetry!r}')
        self.assertEqual(1, len(errors),
                         f'Expected the parser to throw an error for a message with {description}')
        self.assertIsInstance(errors[0], ParserError,
                              f'Expected the parser to throw a ParserError '
                              f'on an message with {description}')

    def _assertParsesOne(self, parser: Parser[T], data, message=None, ignoresBytesHandler=None) -> T:
        """
        Assert that the parser parses exactly one value from the given data.

        :param parser: The parser to use for parsing.
        :param data: The data to hand to the parser.
        :param message: A message for the assertion.
        :param ignoresBytesHandler: An optional handler for ignored bytes.
        :return: The parsed telemetry value.
        """
        if message is None:
            message = ''
        else:
            message = f'{message}: '
        if ignoresBytesHandler is None:
            def ignoresBytesHandler(*_):
                pass
        parsedValue = None
        for telemetry in parser.parse(data, ignoredBytesHandler=ignoresBytesHandler):
            if parsedValue is None:
                parsedValue = telemetry
            else:
                self.fail(f'{message}Expected the parser to generate only one telemetry, '
                          f'got a second one: {telemetry!r}')
        if parsedValue is None:
            self.fail(f'{message}Expected the parser to generate only one telemetry, got nothing')
        return parsedValue

    def _flatten(self, value):
        """
        Yield all child values recursively from the given value.

        :param value: A value.
        """
        if isinstance(value, list):
            for child in value:
                yield from self._flatten(child)
        elif isinstance(value, dict):
            for child in value.values():
                yield from self._flatten(child)
        elif isinstance(value, Enum):
            yield value.value
        else:
            yield value
