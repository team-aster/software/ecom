import os
import struct

from typing import List
from difflib import unified_diff
from inspect import getsourcelines

from ecom.datatypes import TypeInfo
from scripts.ecomUpdate import CCodeGenerator

from tests import EComTestCase


class TestDocumentation(EComTestCase):
    """ Test some aspects of the documentation. """

    def testBaseTypeTableIsUpToDate(self):
        """ Test that the table of base type is up-to-date and update it, if it isn't. """
        baseTypesDocPath = os.path.abspath(os.path.join(
            os.path.dirname(__file__), '..', 'docs', 'database', 'baseTypes.md'))
        lines = []
        currentlyInTable = False
        tableIsUpToDate = True
        columSizes = []
        remainingBaseTypes = list(TypeInfo.BaseType)
        with open(baseTypesDocPath, encoding='utf-8') as docFile:
            for line in docFile:
                if line.startswith('|--'):
                    currentlyInTable = True
                    columSizes = [len(part) for part in line.split('|')[1:-1]]
                elif currentlyInTable:
                    if not line.startswith('|'):
                        currentlyInTable = False
                        if remainingBaseTypes:
                            tableIsUpToDate = False
                            for baseType in remainingBaseTypes:
                                parts = self._getBaseTypeInfo(baseType)
                                lines.append('|' + '|'.join((' ' + part).ljust(size, ' ')
                                                            for part, size in zip(parts, columSizes)) + '|\n')
                    else:
                        parts = [part.strip() for part in line.split('|')[1:-1]]
                        try:
                            baseType = TypeInfo.BaseType(parts[0])
                            remainingBaseTypes.remove(baseType)
                        except ValueError:
                            tableIsUpToDate = False
                            continue  # Base type no longer exists or was already in the table
                        expectedParts = self._getBaseTypeInfo(baseType)
                        if parts != expectedParts:
                            tableIsUpToDate = False
                            line = '|' + '|'.join((' ' + part).ljust(size, ' ')
                                                  for part, size in zip(expectedParts, columSizes)) + '|\n'
                lines.append(line)
        if currentlyInTable and remainingBaseTypes:
            tableIsUpToDate = False
            for baseType in remainingBaseTypes:
                parts = self._getBaseTypeInfo(baseType)
                lines.append('|' + '|'.join((' ' + part).ljust(size, ' ')
                                            for part, size in zip(parts, columSizes)) + '|\n')
        if not tableIsUpToDate:
            with open(baseTypesDocPath, encoding='utf-8') as docFile:
                diff = unified_diff(docFile.readlines(), lines, fromfile='old', tofile='expected')
            with open(baseTypesDocPath, 'w', encoding='utf-8') as docFile:
                docFile.writelines(lines)
            self.fail(f'The base type table in {baseTypesDocPath} was not up to date, '
                      f'it has been edited with the current values.\n\nDiff:\n' + ''.join(diff))

    @staticmethod
    def _getBaseTypeInfo(baseType: TypeInfo.BaseType) -> List[str]:
        """
        Get the information needed for the base type table for a base type.

        :param baseType: The base type whose info should be computed.
        :return: The base type name, size, min value, max value, C type name, Python type name and description.
        """
        pythonType, structFormat, minValue, maxValue = TypeInfo._BASE_TYPE_MAPPING[baseType]
        currentLineIsDoc = False
        nextLineIsDoc = False
        documentation = ''
        for line in getsourcelines(TypeInfo.BaseType)[0]:
            if nextLineIsDoc:
                line = line[line.index('"""') + 3:]
                currentLineIsDoc = True
                nextLineIsDoc = False
            if currentLineIsDoc:
                try:
                    endIndex = line.index('"""')
                except ValueError:
                    endIndex = -1
                documentationLine = line[:endIndex].strip()
                if documentationLine:
                    documentation += documentationLine + ' '
                if endIndex != -1:
                    break
            elif baseType.name in line and baseType.value in line:
                nextLineIsDoc = True
        return [
            baseType.value,
            f'`{struct.calcsize(structFormat)}`',
            '' if minValue is None else f'`{minValue}`',
            '' if maxValue is None else f'`{maxValue}`',
            f'`{CCodeGenerator._BASE_TYPE_C_TYPES[baseType]}`',
            f'`{pythonType.__name__}`',
            documentation.strip()
        ]
