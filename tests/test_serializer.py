from copy import deepcopy
from unittest import TestCase

from ecom.checksum import ChecksumVerifier
from ecom.database import CommunicationDatabase
from ecom.serializer import TelecommandSerializer, TelemetrySerializer

from tests import TEST_DATA_DIR, TEST_DATA_TELEMETRIES


class TestSerializer(TestCase):
    """ Test the serializers. """
    SYNC_BYTE = b'\xaa\x55'

    def setUp(self):
        self._database = CommunicationDatabase(TEST_DATA_DIR)

    def testTelecommandSerializer(self):
        """ Test the telecommand serializer can serialize telecommands. """
        serializer = TelecommandSerializer(self._database, verifier=ChecksumVerifier(self._database))
        self.assertEqual(serializer.nextTelecommandCounter, 0, msg='Expected the first telecommand id to be zero')
        self.assertEqual(
            # SYNC         + checksum    + counter + type
            self.SYNC_BYTE + b'\xc2\x72' + b'\x00' + b'\x01',
            serializer.serialize(self._database.getTelecommandByName('GET_LAST_STATE')),
            'Expected the serializer to be able to serialize a telecommand without arguments')
        self.assertEqual(serializer.nextTelecommandCounter, 1,
                         msg='Expected the next telecommand id to be the last incremented by one')
        self.assertEqual(
            # SYNC         + checksum    + counter + type    + data
            self.SYNC_BYTE + b'\xd9\x2f' + b'\x01' + b'\x10' + b'\x00\x00\x00\x00\x00\x48\x93\x40',
            serializer.serialize(self._database.getTelecommandByName('SET_RPM'), value=1234),
            'Expected the serializer to be able to serialize a telecommand with arguments')
        self.assertEqual(
            # SYNC         + checksum    + counter + type    + data
            self.SYNC_BYTE + b'\xea\xec' + b'\x02' + b'\x03' + b'\xe4\x5c\x00\x00',
            serializer.serialize(self._database.getTelecommandByName('RESET_CHIP')),
            'Expected the serializer to be able to serialize a telecommand with a missing optional argument')
        self.assertEqual(
            # SYNC         + checksum    + counter + type    + data
            self.SYNC_BYTE + b'\xa2\xc1' + b'\x03' + b'\x03' + b'\x34\x12\x00\x00',
            serializer.serialize(self._database.getTelecommandByName('RESET_CHIP'), **{'verification code': 0x1234}),
            'Expected the serializer to be able to serialize a telecommand with a given optional argument')
        currentStateCommand = self._database.getTelecommandByName('SET_CURRENT_STATE')
        STATE_IDLE = currentStateCommand.data[0].type.type['STATE_IDLE']
        self.assertEqual(
            # SYNC         + checksum    + counter + type    + data
            self.SYNC_BYTE + b'\x4a\x6e' + b'\x04' + b'\x04' + b'\x00',
            serializer.serialize(currentStateCommand, state=STATE_IDLE),
            'Expected the serializer to be able to serialize a telecommand with an enum value as an argument')

    def testTelecommandIdWraparound(self):
        """ Test that the telecommand id wraps around when it exceeds its type limit. """
        serializer = TelecommandSerializer(self._database, verifier=ChecksumVerifier(self._database))
        for i in range(256):
            self.assertEqual(serializer.nextTelecommandCounter, i,
                             msg=f'Expected the telecommand #{i} to have the command number {i}')
            serializer.serialize(self._database.getTelecommandByName('GET_LAST_STATE'))
        self.assertEqual(serializer.nextTelecommandCounter, 0,
                         msg=f'Expected the telecommand #256 to have the command number 0')

    def testTelemetrySerializer(self):
        """ Test the telemetry serializer can serialize telemetries. """
        serializer = TelemetrySerializer(self._database, verifier=ChecksumVerifier(self._database))
        heartbeatTelemetry = self._database.getTelemetryByName('HEARTBEAT')
        self.assertEqual(
            # SYNC         + checksum    + type    + data
            self.SYNC_BYTE + b'\x54\x18' + b'\x00' + b'\x34\x12\x00\x00',
            serializer.serialize(heartbeatTelemetry, time=0x1234),
            'Expected the serializer to be able to serialize a telemetry')
        with self.assertRaisesRegex(ValueError, 'Missing value.*"HEARTBEAT".*"time"',
                                    msg='Expect the serializer to throw if a datapoint is not given'):
            serializer.serialize(heartbeatTelemetry)
        messageTelemetry = self._database.getTelemetryByName('MESSAGE')
        message = b'A simple test message'
        self.assertEqual(
            # SYNC         + checksum    + type    + time                + type    + size        + message
            self.SYNC_BYTE + b'\x89\x43' + b'\x03' + b'\x34\x12\x00\x00' + b'\x00' + b'\x15\x00' + message,
            serializer.serialize(messageTelemetry, time=0x1234, type=0, size=len(message), message=message),
            'Expected the serializer to be able to serialize a variable sized telemetry')
        message = 'A test message with a unicode emoji 😀'.encode('utf-8')
        self.assertEqual(
            # SYNC         + checksum    + type    + time                + type    + size        + message
            self.SYNC_BYTE + b'\x80\x34' + b'\x03' + b'\x34\x12\x00\x00' + b'\x01' + b'\x28\x00' + message,
            serializer.serialize(messageTelemetry, time=0x1234, type=1, size=len(message), message=message),
            'Expected the serializer to be able to serialize a variable sized telemetry')

    def testSerializerDoesNotModifyInput(self):
        """ Test that serializing doesn't modify the input data. """
        serializer = TelemetrySerializer(self._database, verifier=ChecksumVerifier(self._database))
        for telemetryName, (_, valuesGenerator) in TEST_DATA_TELEMETRIES.items():
            values = valuesGenerator(self._database)
            telemetryType = self._database.getTelemetryByName(telemetryName)
            valuesBefore = deepcopy(values)
            serializer.serialize(telemetryType, **values)
            self.assertDictEqual(valuesBefore, values)

    def testInfersDynamicSize(self):
        """ Test that serializing creates the correct value for size members if no value is given. """
        serializer = TelemetrySerializer(self._database, verifier=ChecksumVerifier(self._database))
        messageTelemetry = self._database.getTelemetryByName('MESSAGE')
        message = b'A simple test message'
        self.assertEqual(
            # SYNC         + checksum    + type    + time                + type    + size        + message
            self.SYNC_BYTE + b'\x89\x43' + b'\x03' + b'\x34\x12\x00\x00' + b'\x00' + b'\x15\x00' + message,
            serializer.serialize(messageTelemetry, time=0x1234, type=0, message=message),
            'Expected the serializer to be able to infer the size of a variable sized element based on the input')
        self.assertEqual(
            # SYNC         + checksum    + type    + time                + type    + size        + message
            self.SYNC_BYTE + b'\x79\xbd' + b'\x03' + b'\x34\x12\x00\x00' + b'\x00' + b'\x08\x00' + message[:8],
            serializer.serialize(messageTelemetry, time=0x1234, type=0, size=8, message=message),
            'Expected the serializer to use the provided size of a variable sized element, '
            'even if the array is of a different size')
