/** Functionality for sending telemetry. */
// *** AUTOGENERATED FILE - Do not edit manually! - Changes will be overwritten! *** //

#pragma once

#include "generated/units.h"
#include "generated/sharedTypes.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * Send a HEARTBEAT message to the ground station.
 *
 * @param time The time the heartbeat was generated in milliseconds since boot.
 * @return Whether or not the telemetry was send successfully.
 */
extern bool sendTelemetryHeartbeat(ms_t time);

/**
 * Send a RESPONSE message to the ground station.
 *
 * @param commandNumber The counter number of the command that generated this response.
 * @param value The response value of the command.
 * @return Whether or not the telemetry was send successfully.
 */
extern bool sendTelemetryResponse(uint8_t commandNumber, const uint8_t* value);

/**
 * Send a ACKNOWLEDGE message to the ground station.
 *
 * @param commandNumber The counter number of the command that this telemetry acknowledges.
 * @return Whether or not the telemetry was send successfully.
 */
extern bool sendTelemetryAcknowledge(uint8_t commandNumber);

/**
 * Send a MESSAGE message to the ground station.
 *
 * @param time The time the message was generated in milliseconds since the boot.
 * @param type The type of the message.
 * @param size The size of the message in bytes.
 * @param message The message text.
 * @return Whether or not the telemetry was send successfully.
 */
extern bool sendTelemetryMessage(ms_t time, MessageType type, uint16_t size, const char* message);

/**
 * Send a DATA1 message to the ground station.
 *
 * @param time The current system time in milliseconds since boot.
 * @param processorTemperature The temperature of the processor in °C.
 * @param batteryTemperature The temperature read from the thermal sensor which close to the battery
 *                           in °C.
 * @param PduPcbTemperature The temperature read from the thermal sensor which close to the PDU PCB
 *                          in °C.
 * @param motorControllerTemperature The temperature read from the thermal sensor which close to the
 *                                   motor controllers in °C.
 * @param motorBracketTemperature The temperature read from the thermal sensor which close to the
 *                                motor bracket in °C.
 * @param retentionWallTemperature The temperature read from the thermal sensor which close to the
 *                                 retention wall in °C.
 * @param recoveryWallTemperature The temperature read from the thermal sensor which close to the
 *                                recovery wall in °C.
 * @param primaryImuTemperature The temperature of the primary IMU in °C.
 * @param secondaryImuTemperature The temperature of the secondary IMU in °C.
 * @param motorCurrent The current drawn by the motors in ampere.
 * @param secondaryImuRotation The rotations in radians per seconds measured by the secondary IMU.
 * @return Whether or not the telemetry was send successfully.
 */
extern bool sendTelemetryData1(ms_t time, celsius_t processorTemperature,
    celsius_t batteryTemperature, celsius_t PduPcbTemperature, celsius_t motorControllerTemperature,
    celsius_t motorBracketTemperature, celsius_t retentionWallTemperature,
    celsius_t recoveryWallTemperature, celsiusInt8_t primaryImuTemperature,
    celsiusInt8_t secondaryImuTemperature, const Vec3F* motorCurrent,
    const Vec3F* secondaryImuRotation);

/**
 * Send a DATA2 message to the ground station.
 *
 * @param time The current system time in milliseconds since boot.
 * @param primaryXAcceleration The acceleration on the x (roll) axis in meters per second squared
 *                             measured by the primary IMU.
 * @param primaryYAcceleration The acceleration on the y (pitch) axis in meters per second squared
 *                             measured by the primary IMU.
 * @param primaryZAcceleration The acceleration on the z (yaw) axis in meters per second squared
 *                             measured by the primary IMU.
 * @param primaryXMagneticForce The magnetic force on the x (roll) axis in micro Tesla measured by
 *                              the primary IMU.
 * @param primaryYMagneticForce The magnetic force on the y (pitch) axis in micro Tesla measured by
 *                              the primary IMU.
 * @param primaryZMagneticForce The magnetic force on the z (yaw) axis in micro Tesla measured by
 *                              the primary IMU.
 * @param secondaryXAcceleration The acceleration on the x (roll) axis in meters per second squared
 *                               measured by the secondary IMU.
 * @param secondaryYAcceleration The acceleration on the y (pitch) axis in meters per second squared
 *                               measured by the secondary IMU.
 * @param secondaryZAcceleration The acceleration on the z (yaw) axis in meters per second squared
 *                               measured by the secondary IMU.
 * @param secondaryXMagneticForce The magnetic force on the x (roll) axis in micro Tesla measured by
 *                                the secondary IMU.
 * @param secondaryYMagneticForce The magnetic force on the y (pitch) axis in micro Tesla measured
 *                                by the secondary IMU.
 * @param secondaryZMagneticForce The magnetic force on the z (yaw) axis in micro Tesla measured by
 *                                the secondary IMU.
 * @param pressure The current pressure in bar.
 * @param state The current state of the system state machine.
 * @return Whether or not the telemetry was send successfully.
 */
extern bool sendTelemetryData2(ms_t time, mps2_t primaryXAcceleration, mps2_t primaryYAcceleration,
    mps2_t primaryZAcceleration, microTesla_t primaryXMagneticForce,
    microTesla_t primaryYMagneticForce, microTesla_t primaryZMagneticForce,
    mps2_t secondaryXAcceleration, mps2_t secondaryYAcceleration, mps2_t secondaryZAcceleration,
    microTesla_t secondaryXMagneticForce, microTesla_t secondaryYMagneticForce,
    microTesla_t secondaryZMagneticForce, bar_t pressure, SystemState state);

/**
 * Send a DATA3 message to the ground station.
 *
 * @param time The current system time in milliseconds since boot.
 * @param orientation The current orientation calculated by the primary IMU using sensor fusion.
 * @param batteryLevel The voltage level of the FFU battery in volt.
 * @return Whether or not the telemetry was send successfully.
 */
extern bool sendTelemetryData3(ms_t time, const Quaternion* orientation, voltFloat_t batteryLevel);

/**
 * Send a DATA_MOTOR_SPEEDS message to the ground station.
 *
 * @param motorSpeedCount The number of motor speeds in this telemetry package.
 * @param motorSpeeds The speed of the motors in rotations per minute since the last telemetry
 *                    package.
 * @return Whether or not the telemetry was send successfully.
 */
extern bool sendTelemetryDataMotorSpeeds(uint8_t motorSpeedCount, const Vec3FWithTime* motorSpeeds);

/**
 * Send a DATA_ROTATION_SPEEDS message to the ground station.
 *
 * @param rotationSpeedCount The number of rotation speeds in this telemetry package.
 * @param rotationSpeeds The measured rotations in radians per seconds by the primary IMU since the
 *                       last telemetry package.
 * @return Whether or not the telemetry was send successfully.
 */
extern bool sendTelemetryDataRotationSpeeds(uint8_t rotationSpeedCount,
    const Vec3FWithTime* rotationSpeeds);

/**
 * Send a DATA_SET_POINTS message to the ground station.
 *
 * @param setPointsCount The number of set points in this telemetry package.
 * @param setPoints The targeted motor speeds in rotations per minute generated by the controller
 *                  with time stamps since the last telemetry.
 * @return Whether or not the telemetry was send successfully.
 */
extern bool sendTelemetryDataSetPoints(uint8_t setPointsCount, const Vec3Int32WithTime* setPoints);

/**
 * Send a DATA_RECOVERY message to the ground station.
 *
 * @param time The current system time in milliseconds since boot.
 * @param gpsTime The GPS time of week of the navigation epoch in milliseconds.
 * @param longitude Longitude in degrees.
 * @param latitude Latitude in degrees.
 * @param height The height above the ellipsoid in meter.
 * @param recoveryState The current state of the recovery system.
 * @return Whether or not the telemetry was send successfully.
 */
extern bool sendTelemetryDataRecovery(ms_t time, ms_t gpsTime, deg_t longitude, deg_t latitude,
    meter_t height, RecoveryState recoveryState);


#ifdef __cplusplus
}
#endif
