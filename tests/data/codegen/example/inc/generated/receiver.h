/** Functionality to parse incoming messages. */
// *** AUTOGENERATED FILE - Do not edit manually! - Changes will be overwritten! *** //

#pragma once

#include <stddef.h>
#include "generated/units.h"
#include "generated/sharedTypes.h"
#include "generated/bufferAccessor.h"

#ifdef __cplusplus
extern "C" {
#endif

/** An error during parsing of a message. */
typedef enum {
    /** The message type was not recognized. */
    INVALID_MESSAGE,

    /** The checksum of the message was invalid. */
    CHECKSUM_ERROR,

    /** A dynamic size was too large. */
    DYNAMIC_SIZE_TOO_LARGE,
} ParserError;


/** A handler of parsed messages. */
typedef struct MessageHandler {
    /**
     * Handle the SET_LAST_STATE telecommand.
     *
     * @param handler The message handler.
     * @param state The new state that will be set as last state.
     */
    void (*handleCommandSetLastState)(struct MessageHandler* handler, SystemState state);

    /**
     * Handle the GET_LAST_STATE telecommand.
     *
     * @param handler The message handler.
     * @return The last system state.
     */
    SystemState (*handleCommandGetLastState)(struct MessageHandler* handler);

    /**
     * Handle the NEW_LOG_FILE telecommand.
     *
     * @param handler The message handler.
     */
    void (*handleCommandNewLogFile)(struct MessageHandler* handler);

    /**
     * Handle the RESET_CHIP telecommand.
     *
     * @param handler The message handler.
     * @param verificationCode A code that needs to match the predefined value for the chip to get
     *                         reset.
     */
    void (*handleCommandResetChip)(struct MessageHandler* handler, uint32_t verificationCode);

    /**
     * Handle the SET_CURRENT_STATE telecommand.
     *
     * @param handler The message handler.
     * @param state The current state of the system state machine.
     */
    void (*handleCommandSetCurrentState)(struct MessageHandler* handler, SystemState state);

    /**
     * Handle the GET_CURRENT_STATE telecommand.
     *
     * @param handler The message handler.
     * @return The current system state.
     */
    SystemState (*handleCommandGetCurrentState)(struct MessageHandler* handler);

    /**
     * Handle the SET_FLIGHT telecommand.
     *
     * @param handler The message handler.
     * @param verificationCode A code that needs to match the predefined value to confirm the
     *                         change.
     * @param enable Whether or not flight mode should be enabled.
     */
    void (*handleCommandSetFlight)(struct MessageHandler* handler, uint32_t verificationCode,
        bool enable);

    /**
     * Handle the GET_FLIGHT telecommand.
     *
     * @param handler The message handler.
     * @return Whether or not the flight mode is enabled.
     */
    bool (*handleCommandGetFlight)(struct MessageHandler* handler);

    /**
     * Handle the SET_PRE_LAUNCH_TIMER telecommand.
     *
     * @param handler The message handler.
     * @param timer The new value of the pre launch timer in milliseconds since pre-launch started.
     */
    void (*handleCommandSetPreLaunchTimer)(struct MessageHandler* handler, ms_t timer);

    /**
     * Handle the SET_VIDEO_ENABLED telecommand.
     *
     * @param handler The message handler.
     * @param enabled Whether or not the GoPros should be enabled.
     */
    void (*handleCommandSetVideoEnabled)(struct MessageHandler* handler, bool enabled);

    /**
     * Handle the RESET_RECOVERY telecommand.
     *
     * @param handler The message handler.
     */
    void (*handleCommandResetRecovery)(struct MessageHandler* handler);

    /**
     * Handle the MOTOR_ENABLED telecommand.
     *
     * @param handler The message handler.
     * @param enabled Whether or not the motors should be enabled.
     */
    void (*handleCommandMotorEnabled)(struct MessageHandler* handler, bool enabled);

    /**
     * Handle the MOTOR_WARMUP telecommand.
     *
     * @param handler The message handler.
     * @param enabled Whether or not the motor warmup sequence should be enabled.
     */
    void (*handleCommandMotorWarmup)(struct MessageHandler* handler, bool enabled);

    /**
     * Handle the SET_PWM telecommand.
     *
     * @param handler The message handler.
     * @param value The new pwm value for the motors.
     */
    void (*handleCommandSetPwm)(struct MessageHandler* handler, pwm_t value);

    /**
     * Handle the SET_RPM telecommand.
     *
     * @param handler The message handler.
     * @param value The new rpm value for the motors.
     */
    void (*handleCommandSetRpm)(struct MessageHandler* handler, rpm_t value);

    /**
     * Handle the SET_ADCS_MODE telecommand.
     *
     * @param handler The message handler.
     * @param mode The new ADCS mode.
     */
    void (*handleCommandSetAdcsMode)(struct MessageHandler* handler, AdcsMode mode);

    /**
     * Handle the DEPLOY_PARACHUTE telecommand.
     *
     * @param handler The message handler.
     */
    void (*handleCommandDeployParachute)(struct MessageHandler* handler);

    /**
     * Handle the SEND_IRIDIUM_MESSAGE telecommand.
     *
     * @param handler The message handler.
     */
    void (*handleCommandSendIridiumMessage)(struct MessageHandler* handler);

    /**
     * Handle the CALIBRATE_IMU telecommand.
     *
     * @param handler The message handler.
     * @param imu The IMU to calibrate.
     * @return Whether or not the calibration was successful.
     */
    bool (*handleCommandCalibrateImu)(struct MessageHandler* handler, uint8_t imu);

    /**
     * Handle the SET_PAYLOAD_VIDEO telecommand.
     *
     * @param handler The message handler.
     * @param enabled Whether or not the payload should record video.
     */
    void (*handleCommandSetPayloadVideo)(struct MessageHandler* handler, bool enabled);

    /**
     * Handle the SET_RECOVERY_STATE telecommand.
     *
     * @param handler The message handler.
     * @param state The target state of the recovery system.
     */
    void (*handleCommandSetRecoveryState)(struct MessageHandler* handler, RecoveryState state);

    /**
     * Handle the SEND_COMMAND telecommand.
     *
     * @param handler The message handler.
     * @param size The size of the command in bytes.
     * @param command The shell command that will be executed.
     * @return The exit code of the executed shell commands.
     */
    int32_t (*handleCommandSendCommand)(struct MessageHandler* handler, uint16_t size,
        const char* command);

    /**
     * Handle an error during parsing of a message.
     *
     * @param handler The message handler.
     * @param error The error that occurred.
     */
    void (*errorHandler)(struct MessageHandler* handler, ParserError error);

    /** Private data of the message handler. */
    void* data;
} MessageHandler;


/**
 * Parse and handle a received message.
 *
 * @param buffer An accessor to a buffer to parse a message from.
 * @param handler An handler for parsed messages.
 * @return Whether the message was parsed and handled.
 */
extern bool parseMessage(BufferAccessor* buffer, MessageHandler* handler);

#ifdef __cplusplus
}
#endif
