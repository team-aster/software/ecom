import struct

from unittest.mock import MagicMock

from ecom.parser import TelemetryParser, ParserError, TelecommandParser
from ecom.message import Telemetry
from ecom.checksum import calculateChecksum, ChecksumVerifier
from ecom.database import CommunicationDatabase
from ecom.response import ResponseTelemetryParser

from tests import TEST_DATA_DIR, TEST_DATA_TELEMETRIES, TEST_DATA_TELECOMMANDS, EComTestCase


class SilentTelemetryParser(TelemetryParser):
    """ A telemetry parser that silently ignores skipped bytes. """
    def _defaultIgnoredBytesHandler(self, buffer: bytes, ignoredBytes: int):
        pass


class TestTelemetryParser(EComTestCase):
    """ Test the telemetry parser. """
    _VALID_TELEMETRY = b'\xaa\x55\x97\xD0\x03\x15\xcd\x5b\x07\x01\x04\x00Test'

    def setUp(self):
        self.maxDiff = None
        self._database = CommunicationDatabase(TEST_DATA_DIR)

    def testIgnoresGarbage(self):
        """ Test the parser ignores garbage data. """
        parser = TelemetryParser(self._database, verifier=ChecksumVerifier(self._database))
        garbageBytes = b'garbage' * 20
        ignoredBytesHandler = MagicMock(return_value=None)
        # noinspection PyTypeChecker
        for telemetry in parser.parse(garbageBytes, ignoredBytesHandler=ignoredBytesHandler):
            self.fail(f'Expected the parser not to read a telemetry from garbage data, got {telemetry!r}')
        ignoredBytesHandler.assert_called_once_with(garbageBytes, len(garbageBytes) - 1)
        ignoredBytesHandler.reset_mock()
        self._assertParsesOne(parser, self._VALID_TELEMETRY,
                              'Expected the parser to be able to read a valid message after garbage',
                              ignoresBytesHandler=ignoredBytesHandler)
        ignoredBytesHandler.assert_called_once_with(garbageBytes[-1:] + self._VALID_TELEMETRY, 1)

    def testHandlesInvalidTelemetryType(self):
        """ Test that the parser raises an error if the telemetry type is invalid. """
        parser = SilentTelemetryParser(self._database, verifier=ChecksumVerifier(self._database))
        startParsedBytes = parser.numParsedBytes
        startInvalidBytes = parser.numInvalidBytes
        self._assertParserError(parser, b'\xaa\x55\x00\x00\xffGarbage', 'an invalid telemetry type')
        with self.assertRaisesRegex(ParserError, 'is not a valid TelemetryType') as exceptionCatcher:
            for telemetry in parser.parse(b'\xaa\x55\x00\x00\xffGarbage'):
                self.fail(f'Expected the parser not to read a telemetry from a message '
                          f'with an invalid telemetry type and no error handler, got {telemetry!r}')
        self.assertEqual(exceptionCatcher.exception.buffer, b'\xaa\x55\x00\x00\xffGarbage',
                         msg='Expected the ParseException to contain the entire parser buffer')
        self.assertEqual(startParsedBytes, parser.numParsedBytes,
                         'Expected the parser not to increase the parsed bytes '
                         'for a message with an invalid telemetry type')
        self.assertEqual(startInvalidBytes + 13, parser.numInvalidBytes,
                         'Expected the parser to count the bytes of a message '
                         'with an invalid telemetry type as invalid bytes')
        self._assertParsesOne(parser, self._VALID_TELEMETRY,
                              'Expected the parser to be able to read a valid message '
                              'after an invalid telemetry type with no error handler')

    def testHandlesInvalidChecksum(self):
        """ Test that the parser raises an error if the checksum is not valid. """
        parser = SilentTelemetryParser(self._database, verifier=ChecksumVerifier(self._database))
        startParsedBytes = parser.numParsedBytes
        startInvalidBytes = parser.numInvalidBytes
        messageWithInvalidChecksum = b'\xaa\x55\x01\x00\x00' + b'\x00' * 201 + b'Garbage'
        self._assertParserError(parser, messageWithInvalidChecksum, 'an invalid checksum')
        self.assertEqual(startParsedBytes, parser.numParsedBytes,
                         'Expected the parser not to increase the parsed bytes for a message with an invalid checksum')
        self.assertEqual(startInvalidBytes + 212, parser.numInvalidBytes,
                         'Expected the parser to count the bytes of a message '
                         'with an invalid checksum as invalid bytes')
        self._assertParsesOne(parser, self._VALID_TELEMETRY,
                              'Expected the parser to be able to read a valid message after an invalid checksum')
        # Verify that the invalid checksum is ignored if no verifier is given to the parser
        self._assertParsesOne(TelemetryParser(self._database), messageWithInvalidChecksum)

    def testBrokenUpParsing(self):
        """ Test that the parser handles messages that arrive in chunks. """
        parser = SilentTelemetryParser(self._database, verifier=ChecksumVerifier(self._database))
        self.assertIsNone(next(parser.parse(b'garbage' * 20 + self._VALID_TELEMETRY[:1]), None),
                          'Expected the parser not to parse anything from an incomplete message')
        self.assertIsNone(next(parser.parse(self._VALID_TELEMETRY[1:5]), None),
                          'Expected the parser not to parse anything from an incomplete message')
        self.assertIsNotNone(next(parser.parse(self._VALID_TELEMETRY[5:]), None),
                             'Expected the parser to be able to parse a message that arrives in multiple chunks')

    def testSuccessiveParsing(self):
        """ Test that the parser handles messages that arrive directly after each other in one chunk. """
        parser = SilentTelemetryParser(self._database, verifier=ChecksumVerifier(self._database))
        telemetry = Telemetry(
            type=self._database.getTelemetryByName('MESSAGE').id,
            data={
                'size': 4,
                'message': b'Test',
                'type': self._database.dataTypes['MessageType'].type['INFO_MESSAGE'],
                'time': 123456789,
            },
            header={
                'checksum': 53399,
                'sync byte 1': 170,
                'sync byte 2': 85,
                'type': self._database.getTelemetryByName('MESSAGE').id,
            }
        )
        self.assertListEqual(list(parser.parse(self._VALID_TELEMETRY + self._VALID_TELEMETRY)), [telemetry, telemetry],
                             'Expected the parser to be able to parse two successive telemetry messages in one chunk')

    def testTelemetryDataParsing(self):
        """ Test the parser handles DATA telemetry messages. """
        parser = SilentTelemetryParser(self._database, verifier=ChecksumVerifier(self._database))
        for telemetryName, (binaryFormat, valuesGenerator) in TEST_DATA_TELEMETRIES.items():
            values = valuesGenerator(self._database)
            telemetryType = self._database.telemetryTypeEnum[telemetryName]
            # noinspection PyTypeChecker
            payload = bytes([telemetryType.value]) + struct.pack(binaryFormat, *self._flatten(values))
            telemetry = self._assertParsesOne(parser, self._makePackage(payload))
            self.assertIs(telemetry.type, telemetryType, f'Expected the parser to return a {telemetryName} message')
            self.assertListEqual(list(values.keys()), list(telemetry.data.keys()),
                                 f'The parser returned unexpected data for a {telemetryName} message')
            for name, value in values.items():
                self._assertAlmostEqual(value, telemetry.data[name], delta=0.01,
                                        msg=f'The parser returned unexpected data for the {name} '
                                            f'item in a {telemetryName} message')
            # Test handles invalid data
            startParsedBytes = parser.numParsedBytes
            startInvalidBytes = parser.numInvalidBytes
            self._assertParserError(parser, b'\xaa\x55\x00\x00\x05' + bytes(bytearray(range(61))), 'invalid data')
            self.assertEqual(startParsedBytes, parser.numParsedBytes,
                             'Expected the parser not to increase the parsed bytes for a message with invalid data')
            self.assertEqual(startInvalidBytes + 65, parser.numInvalidBytes,
                             'Expected the parser to count the bytes of a message with invalid type as invalid bytes')
            self._assertParsesOne(parser, self._VALID_TELEMETRY,
                                  'Expected the parser to be able to read a valid message '
                                  'after an message with invalid data')

    def testTelemetryMessageParsing(self):
        """ Test the parser handles MESSAGE telemetry messages. """
        parser = SilentTelemetryParser(self._database, verifier=ChecksumVerifier(self._database))
        message = b'This is a test message'
        values = {
            'time': 123456789,
            'type': self._database.dataTypes['MessageType'].type['INFO_MESSAGE'],
            'size': len(message),
            'message': message,
        }
        payload = b'\x03' + struct.pack(f'<IBH{len(message)}s', *self._flatten(values))
        telemetry = self._assertParsesOne(parser, self._makePackage(payload))
        self.assertIs(telemetry.type, self._database.telemetryTypeEnum['MESSAGE'],
                      'Expected the parser to return a MESSAGE telemetry')
        self.assertDictEqual(values, telemetry.data, 'The parser returned unexpected data for a MESSAGE telemetry')

    def testTelemetryResponseParsing(self):
        """ Test the parser handles RESPONSE telemetry messages. """
        MAX_CONFIG_VALUE_SIZE = self._database.constants['MAX_CONFIG_VALUE_SIZE'].value
        parser = SilentTelemetryParser(self._database, verifier=ChecksumVerifier(self._database))
        value = (b'\x42' * 8).ljust(MAX_CONFIG_VALUE_SIZE, b'\x00')
        values = {
            'command number': 42,
            'value': value,
        }
        payload = b'\x01' + struct.pack(f'<B{len(value)}s', *self._flatten(values))
        telemetry = self._assertParsesOne(parser, self._makePackage(payload))
        self.assertIs(telemetry.type, self._database.telemetryTypeEnum['RESPONSE'],
                      'Expected the parser to return a RESPONSE telemetry')
        self.assertDictEqual(values, telemetry.data, 'The parser returned unexpected data for a RESPONSE telemetry')

    def testTelecommandParsing(self):
        """ Test the parser handles telecommand messages. """
        parser = TelecommandParser(self._database, verifier=ChecksumVerifier(self._database))
        payloadFormat, payloadGenerator = TEST_DATA_TELECOMMANDS['NEW_LOG_FILE']
        payload = struct.pack(payloadFormat, *self._flatten(payloadGenerator(self._database, counter=0)))
        telecommand = self._assertParsesOne(parser, self._makePackage(payload))
        self.assertIs(telecommand.type, self._database.telecommandTypeEnum['NEW_LOG_FILE'],
                      'Expected the parser to return a NEW_LOG_FILE telecommand')
        self.assertDictEqual(telecommand.data, {},
                             'The parser returned unexpected data for a NEW_LOG_FILE telecommand')

        payloadFormat, payloadGenerator = TEST_DATA_TELECOMMANDS['RESET_CHIP']
        values = payloadGenerator(self._database, counter=1)
        payload = struct.pack(payloadFormat, *self._flatten(values))
        telecommand = self._assertParsesOne(parser, self._makePackage(payload))
        self.assertIs(telecommand.type, self._database.telecommandTypeEnum['RESET_CHIP'],
                      'Expected the parser to return a RESET_CHIP telecommand')
        self.assertDictEqual({'verification code': values['verification code']}, telecommand.data,
                             'The parser returned unexpected data for a RESET_CHIP telecommand')

    def testConfigParsing(self):
        """ Test that both parsers can parse config messages. """
        MAX_CONFIG_VALUE_SIZE = self._database.constants['MAX_CONFIG_VALUE_SIZE'].value
        parser = TelecommandParser(self._database, verifier=ChecksumVerifier(self._database))
        payloadFormat, payloadGenerator = TEST_DATA_TELECOMMANDS['SET_CONFIG']
        values = payloadGenerator(self._database, counter=0)
        payload = struct.pack(payloadFormat, *self._flatten(values))
        telecommand = self._assertParsesOne(parser, self._makePackage(payload))
        self.assertIs(telecommand.type, self._database.telecommandTypeEnum['SET_CONFIG'],
                      'Expected the parser to return a SET_CONFIG telecommand')
        self.assertDictEqual(telecommand.data, {
            'config': values['config'],
            'value': values['value'],
        }, 'The parser returned unexpected data for a SET_CONFIG telecommand')

        parser = ResponseTelemetryParser(self._database, verifier=ChecksumVerifier(self._database))
        parser.serialize(self._database.getTelecommandByName('GET_CONFIG'),
                         config=self._database.configurationEnum['parachute trigger altitude'])
        value = 1.2345
        values = {
            'command number': 0,
            'value': value,
        }
        payload = b'\x01' + struct.pack(f'<Bd', *self._flatten(values)).ljust(MAX_CONFIG_VALUE_SIZE + 1, b'\x00')
        telemetry = self._assertParsesOne(parser, self._makePackage(payload))
        self.assertIs(telemetry.type, self._database.telemetryTypeEnum['RESPONSE'],
                      'Expected the parser to return a RESPONSE telemetry')
        self.assertDictEqual(telemetry.data, {
            'command number': 0,
            'value': value,
            'command': self._database.getTelecommandByName('GET_CONFIG'),
        }, 'The parser returned unexpected data for a RESPONSE telemetry')

    def testParsingStatistics(self):
        """ Test the parser correctly collects parsing statistics. """
        parser = SilentTelemetryParser(self._database, verifier=ChecksumVerifier(self._database))
        self.assertEqual(parser.numParsedBytes, 0, 'Expected the number of parsed bytes to be zero at the start')
        self.assertEqual(parser.numInvalidBytes, 0, 'Expected the number of invalid bytes to be zero at the start')
        garbage = b'garbage' * 20
        next(parser.parse(garbage), None)
        self.assertEqual(parser.numParsedBytes, 0,
                         'Expected the parser not to increase the parsed bytes, if no message was parsed')
        self.assertEqual(parser.numInvalidBytes, len(garbage) - 1,
                         'Expected the parser note all garbage bytes as invalid bytes except the last one')
        self._assertParsesOne(parser, self._VALID_TELEMETRY)
        self.assertEqual(parser.numParsedBytes, len(self._VALID_TELEMETRY),
                         'Expected the parser to increase the parsed bytes by the number of bytes in the message')
        self.assertEqual(parser.numInvalidBytes, len(garbage),
                         'Expected the parser not correctly count the number of invalid bytes')

    def testParsingTooLargeDynamicallySizedMessage(self):
        """ Test the parser rejects a message that contains a dynamically sized member which exceeds the limit. """
        verifier = ChecksumVerifier(self._database)
        parsers = {
            TelemetryParser.DEFAULT_MAX_DYNAMIC_MEMBER_SIZE: SilentTelemetryParser(self._database, verifier=verifier),
            256: SilentTelemetryParser(self._database, verifier=verifier, maxDynamicMemberSize=256),
        }
        for maxSize, parser in parsers.items():
            bad_size_values = {
                'time': 123456789,
                'type': self._database.dataTypes['MessageType'].type['INFO_MESSAGE'],
                'size': maxSize + 1,
                'message': b'This is a test message with a bad size',
            }
            payload = b'\x03' + struct.pack(f'<IBH{len(bad_size_values["message"])}s', *self._flatten(bad_size_values))
            parserError = None

            def errorHandler(error):
                nonlocal parserError
                parserError = error

            telemetry = list(parser.parse(self._makePackage(payload), errorHandler=errorHandler))
            self.assertListEqual(telemetry, [], f'Expected the parser to ignore a dynamically sized telemetry '
                                                f'with an invalid size (>{maxSize})')
            self.assertIsInstance(
                parserError, ParserError, f'Expected the parser to report an error about the dynamically '
                                          f'sized telemetry with an invalid size (>{maxSize})')
            self.assertRegex(str(parserError), f'size.*{bad_size_values["size"]}',
                             f'Expected the error message to mention the invalid size (>{maxSize})')

            # Test that the parser can parse a valid message after an invalid dynamically sized message
            message = b'This is a test message with a good size'
            good_size_values = {
                'time': 123456789,
                'type': self._database.dataTypes['MessageType'].type['INFO_MESSAGE'],
                'size': len(message),
                'message': message,
            }
            payload = b'\x03' + struct.pack(f'<IBH{len(good_size_values["message"])}s',
                                            *self._flatten(good_size_values))
            telemetry = self._assertParsesOne(parser, self._makePackage(payload))
            self.assertIs(
                telemetry.type, self._database.telemetryTypeEnum['MESSAGE'],
                f'Expected the parser to return a MESSAGE telemetry after an invalid sized message (>{maxSize})')
            self.assertDictEqual(good_size_values, telemetry.data,
                                 f'The parser returned unexpected data for a MESSAGE telemetry '
                                 f'after an invalid sized message (>{maxSize}')

    @staticmethod
    def _makePackage(payload):
        """
        Create a message package.

        :param payload: The payload of the package.
        :return: The assembled package.
        """
        return b'\xaa\x55' + struct.pack('<H', calculateChecksum(payload)) + payload
