from ecom.parser import TelecommandParser, ParserError
from ecom.message import Telemetry, Telecommand, TelemetryType, TelemetryDatapointType
from ecom.checksum import ChecksumVerifier
from ecom.database import CommunicationDatabase
from ecom.response import ResponseTelemetryParser, FixedSizeResponseTelemetrySerializer, \
    VariableSizedResponseTelemetrySerializer
from ecom.datatypes import TypeInfo

from tests import TEST_DATA_DIR, EComTestCase, TEST_DATA_TELECOMMANDS


class TestResponseCommunication(EComTestCase):
    """ Test the response parsing and serializing. """

    def setUp(self):
        self.maxDiff = None
        self._database = CommunicationDatabase(TEST_DATA_DIR)

    def testParseTelecommandResponses(self):
        """ Test that the ResponseTelemetryParser can parse the response packages for telecommands correctly. """
        verifier = ChecksumVerifier(self._database)
        parser = ResponseTelemetryParser(self._database, verifier=verifier)
        telecommandParser = TelecommandParser(self._database, verifier=verifier)
        telecommandSerializer = FixedSizeResponseTelemetrySerializer(self._database, verifier=verifier)
        TELEMETRY_ACKNOWLEDGE_ID = self._database.telemetryTypeEnum['ACKNOWLEDGE']
        TELEMETRY_ACKNOWLEDGE = self._database.getTelemetryByName('ACKNOWLEDGE')
        TELEMETRY_RESPONSE_ID = self._database.telemetryTypeEnum['RESPONSE']
        QuaternionArray = self._database.parseKnownTypeInfo('Quaternion[2]').type
        Quaternion = self._database.dataTypes['Quaternion'].type
        EXPECTED_RESPONSES = {
            'NEW_LOG_FILE': Telemetry(
                type=TELEMETRY_ACKNOWLEDGE_ID,
                data={
                    'command': self._database.getTelecommandByName('NEW_LOG_FILE'),
                    'command number': 0,
                },
                header={
                    'checksum': 38809,
                    'sync byte 1': 170,
                    'sync byte 2': 85,
                    'type': TELEMETRY_ACKNOWLEDGE_ID,
                }
            ),
            'GET_LAST_STATE': Telemetry(
                type=TELEMETRY_RESPONSE_ID,
                data={
                    'command': self._database.getTelecommandByName('GET_LAST_STATE'),
                    'command number': 1,
                    'value': self._database.dataTypes['SystemState'].type['STATE_LAUNCH'],
                },
                header={
                    'checksum': 2960,
                    'sync byte 1': 170,
                    'sync byte 2': 85,
                    'type': TELEMETRY_RESPONSE_ID,
                }
            ),
            'GET_CONFIG': Telemetry(
                type=TELEMETRY_RESPONSE_ID,
                data={
                    'command': self._database.getTelecommandByName('GET_CONFIG'),
                    'command number': 2,
                    'value': QuaternionArray([Quaternion({'x': 1, 'y': 2, 'z': 3, 'w': 4}),
                                              Quaternion({'x': 5, 'y': 6, 'z': 7, 'w': 8})]),
                },
                header={
                    'checksum': 39249,
                    'sync byte 1': 170,
                    'sync byte 2': 85,
                    'type': TELEMETRY_RESPONSE_ID,
                }
            ),
        }
        for telecommandName, expectedResponse in EXPECTED_RESPONSES.items():
            _, valuesGenerator = TEST_DATA_TELECOMMANDS[telecommandName]
            values = {name: value for name, value in valuesGenerator(self._database, counter=0).items()
                      if name != 'counter' and name != 'type'}
            telecommandType = self._database.getTelecommandByName(telecommandName)
            telecommand = next(telecommandParser.parse(parser.serialize(telecommandType, **values)))
            if expectedResponse.type is TELEMETRY_ACKNOWLEDGE_ID:
                payload = telecommandSerializer.serialize(
                    TELEMETRY_ACKNOWLEDGE, **{'command number': telecommand.header['counter']})
            else:
                payload = telecommandSerializer.serializeTelecommandResponse(
                    telecommand, expectedResponse.data['value'])
            response = self._assertParsesOne(
                parser, payload, 'Expected a ResponseTelemetryParser to parse exactly one '
                                 'telecommand response from the payload of one telecommand response')
            self.assertEqual(expectedResponse, response, 'Expected a ResponseTelemetryParser '
                                                         'to correctly parse a telecommand response')

    def testSerializingFixedSizeTelecommandResponses(self):
        """ Test that the FixedSizeResponseTelemetrySerializer can serialize the response packages correctly. """
        verifier = ChecksumVerifier(self._database)
        parser = FixedSizeResponseTelemetrySerializer(self._database, verifier=verifier)
        TEST_CASES = [(
            Telecommand(
                type=self._database.telecommandTypeEnum['GET_LAST_STATE'],
                data={},
                header={
                    'counter': 0
                },
            ),
            self._database.dataTypes['SystemState'].type['STATE_LAUNCH'],
            # sync      + checksum    + type    + counter + result  + padding
            b'\xaa\x55' + b'\xa4\x7c' + b'\x01' + b'\x00' + b'\x02' + b'\x00' * 63,
        ), (
            Telecommand(
                type=self._database.telecommandTypeEnum['GET_CONFIG'],
                data={
                    'config': self._database.dataTypes['ConfigurationId'].type['stable pressure landed duration'],
                },
                header={
                    'counter': 1
                },
            ),
            0x123456,
            # sync      + checksum    + type    + counter + result              + padding
            b'\xaa\x55' + b'\xb6\x6c' + b'\x01' + b'\x01' + b'\x56\x34\x12\x00' + b'\x00' * 60,
        ), (
            Telecommand(
                type=self._database.telecommandTypeEnum['GET_CONFIG'],
                data={
                    'config': self._database.dataTypes['ConfigurationId'].type['log file path'],
                },
                header={
                    'counter': 2
                },
            ),
            b'/test/path/that/to/logfile.txt',
            # sync      + checksum    + type    + counter + result                            + padding
            b'\xaa\x55' + b'\x84\x61' + b'\x01' + b'\x02' + b'/test/path/that/to/logfile.txt' + b'\x00' * 34,
        )]
        for telecommand, value, expectedResult in TEST_CASES:
            result = parser.serializeTelecommandResponse(telecommand, value)
            self.assertEqual(expectedResult, result, 'Expected a FixedSizeResponseTelemetrySerializer '
                                                     'to serialize a telecommand response correctly')

    def testSerializingVariableSizedTelecommandResponses(self):
        """ Test that the VariableSizedResponseTelemetrySerializer can serialize the response packages correctly. """
        database = CommunicationDatabase(TEST_DATA_DIR)
        responseTelemetryId = database.telemetryTypeEnum['RESPONSE']
        variableSizedResponseTelemetry = TelemetryType(
            id=responseTelemetryId,
            data=[
                TelemetryDatapointType(name='command number', type=TypeInfo.lookupBaseType(TypeInfo.BaseType.UINT8)),
                TelemetryDatapointType(name='size', type=TypeInfo.lookupBaseType(TypeInfo.BaseType.UINT8)),
                TelemetryDatapointType(name='value', type=database.parseKnownTypeInfo('bytes[.size]', 'value')),
            ]
        )
        for i, telemetryType in enumerate(database.telemetryTypes):
            if telemetryType.id is responseTelemetryId:
                break
        else:
            self.fail('Expected to find a RESPONSE telemetry type in the test database')
        database.telemetryTypes[i] = variableSizedResponseTelemetry
        verifier = ChecksumVerifier(database)
        parser = VariableSizedResponseTelemetrySerializer(database, verifier=verifier)
        TEST_CASES = [(
            Telecommand(
                type=database.telecommandTypeEnum['GET_LAST_STATE'],
                data={},
                header={
                    'counter': 0
                },
            ),
            database.dataTypes['SystemState'].type['STATE_LAUNCH'],
            # sync      + checksum    + type    + counter + size    + result
            b'\xaa\x55' + b'\x88\x2e' + b'\x01' + b'\x00' + b'\x01' + b'\x02',
        ), (
            Telecommand(
                type=database.telecommandTypeEnum['GET_CONFIG'],
                data={
                    'config': database.dataTypes['ConfigurationId'].type['stable pressure landed duration'],
                },
                header={
                    'counter': 1
                },
            ),
            0x123456,
            # sync      + checksum    + type    + counter + size    + result
            b'\xaa\x55' + b'\x20\x26' + b'\x01' + b'\x01' + b'\x04' + b'\x56\x34\x12\x00',
        ), (
            Telecommand(
                type=database.telecommandTypeEnum['GET_CONFIG'],
                data={
                    'config': database.dataTypes['ConfigurationId'].type['log file path'],
                },
                header={
                    'counter': 2
                },
            ),
            b'/test/path/that/to/logfile.txt',
            # sync      + checksum    + type    + counter + size    + result
            b'\xaa\x55' + b'\x70\x08' + b'\x01' + b'\x02' + b'\x40' + b'/test/path/that/to/logfile.txt' + b'\x00' * 34,
        )]
        for telecommand, value, expectedResult in TEST_CASES:
            result = parser.serializeTelecommandResponse(telecommand, value)
            self.assertEqual(expectedResult, result, 'Expected a VariableSizedResponseTelemetrySerializer '
                                                     'to serialize a telecommand response correctly')

    def testHandlesResponsesForMissingCommandsType(self):
        """ Test that the parser raises an error if the response telemetry references an unknown telecommand. """
        verifier = ChecksumVerifier(self._database)
        parser = ResponseTelemetryParser(self._database, verifier=verifier)
        serializer = FixedSizeResponseTelemetrySerializer(self._database, verifier=verifier)
        payloads = {
            'acknowledgement': serializer.serializeTelecommandAcknowledge(Telecommand(
                self._database.getTelecommandByName('NEW_LOG_FILE').id, data={}, header={'counter': 42})),
            'response': serializer.serializeTelecommandResponse(Telecommand(
                self._database.getTelecommandByName('GET_FLIGHT').id, data={}, header={'counter': 42}), True),
        }
        for responseType, payload in payloads.items():
            with self.assertRaisesRegex(
                    ParserError, '^Received .* with an invalid command number 42$') as exceptionCatcher:
                for telemetry in parser.parse(payload):
                    self.fail(f'Expected the parser not to read a {responseType} from a message '
                              f'with an invalid telecommand reference, got {telemetry!r}')
            self.assertEqual(exceptionCatcher.exception.buffer, payload,
                             msg='Expected the ParseException to contain the entire parser buffer')
