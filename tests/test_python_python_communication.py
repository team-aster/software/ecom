from typing import Dict, Any

from ecom.parser import TelemetryParser, TelecommandParser
from ecom.message import MessageType
from ecom.checksum import ChecksumVerifier
from ecom.database import CommunicationDatabase
from ecom.serializer import TelemetrySerializer, TelecommandSerializer

from tests import TEST_DATA_DIR, TEST_DATA_TELEMETRIES, EComTestCase, TEST_DATA_TELECOMMANDS


class TestPythonToPythonCommunication(EComTestCase):
    """ Test the communication from a Python ECom module to another Python ECom module. """

    def setUp(self):
        self.maxDiff = None
        self._database = CommunicationDatabase(TEST_DATA_DIR)

    def testTelemetrySerializationAndDeserialization(self):
        """ Test that the telemetry parser can parse the packages the serializer generates from telemetry. """
        verifier = ChecksumVerifier(self._database)
        serializer = TelemetrySerializer(self._database, verifier=verifier)
        parser = TelemetryParser(self._database, verifier=verifier)
        for telemetryName, (_, valuesGenerator) in TEST_DATA_TELEMETRIES.items():
            values = valuesGenerator(self._database)
            telemetryType = self._database.getTelemetryByName(telemetryName)
            self._assertParserMatchesSerializer(parser, serializer, telemetryType, values)

    def testTelecommandSerializationAndDeserialization(self):
        """ Test that the telecommand parser can parse the packages the serializer generates from telecommands. """
        verifier = ChecksumVerifier(self._database)
        serializer = TelecommandSerializer(self._database, verifier=verifier)
        parser = TelecommandParser(self._database, verifier=verifier)
        for telecommandName, (_, valuesGenerator) in TEST_DATA_TELECOMMANDS.items():
            values = valuesGenerator(self._database, counter=0)
            telecommandType = self._database.getTelecommandByName(telecommandName)
            self._assertParserMatchesSerializer(
                parser, serializer, telecommandType,
                {name: value for name, value in values.items() if name not in ('counter', 'type')})

    def _assertParserMatchesSerializer(self, parser, serializer, messageType: MessageType, values: Dict[str, Any]):
        """
        Assert that the parser parses the values from the payload that the serializer produced for the same values.

        :param parser: The parser to test.
        :param serializer: The serializer to test.
        :param messageType: The type of the message that is parsed and serialized.
        :param values: The values that are passed as data with the message.
        """
        payload = serializer.serialize(messageType, **values)
        telecommand = self._assertParsesOne(parser, payload)
        self.assertIs(telecommand.type, messageType.id)
        for name, value in values.items():
            self._assertAlmostEqual(value, telecommand.data[name], delta=0.01,
                                    msg=f'The parser returned unexpected data for the {name} '
                                        f'item in a {messageType.id.name} message')
