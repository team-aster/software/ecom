from typing import Optional, Union

from ecom.database import CommunicationDatabase
from ecom.datatypes import StructType, structDataclass, structField, TypeInfo, EnumType, ArrayType

from tests import TEST_DATA_DIR, EComTestCase


class TestDatatypes(EComTestCase):
    """ Test the datatypes functionality. """

    def testStructDataclassDecorator(self):
        """ Test that the structDataclass decorator functionality. """
        database = CommunicationDatabase(TEST_DATA_DIR)
        floatTypeInfo = TypeInfo.lookupBaseType(TypeInfo.BaseType.FLOAT)

        @structDataclass(database)
        class Quaternion(StructType):
            x: float = structField(typ=floatTypeInfo)
            y: float = structField(typ=floatTypeInfo)
            z: float = structField(typ=floatTypeInfo)
            w: float = structField(typ=floatTypeInfo)

        self.assertEqual(str(Quaternion(x=1, y=2, z=3, w=4))[-38:], 'Quaternion(x=1.0, y=2.0, z=3.0, w=4.0)',
                         'Expected a class with a structDataclass decorator '
                         'to generate a standard dataclass __str__ method')
        self.assertEqual(Quaternion(x=1, y=2, z=3, w=4).x, 1,
                         'Expected a class with a structDataclass decorator to provide an accessor to the fields')
        self.assertEqual(str(Quaternion({'x': 1, 'y': 2, 'z': 3, 'w': 4}))[-38:],
                         'Quaternion(x=1.0, y=2.0, z=3.0, w=4.0)',
                         'Expected a class with a structDataclass decorator '
                         'to generate a standard dataclass __str__ method')
        self.assertEqual(Quaternion({'x': 1, 'y': 2, 'z': 3, 'w': 4}).x, 1,
                         'Expected a class with a structDataclass decorator to provide an accessor to the fields')

        @structDataclass(database)
        class QuaternionWithExtra(StructType):
            x: float = structField(typ=floatTypeInfo)
            y: float = structField(typ=floatTypeInfo)
            z: float = structField(typ=floatTypeInfo)
            w: float = structField(typ=floatTypeInfo)
            a: float = 0.1

        self.assertEqual(str(QuaternionWithExtra(x=1, y=2, z=3, w=4))[-54:],
                         'QuaternionWithExtra(x=1.0, y=2.0, z=3.0, w=4.0, a=0.1)',
                         'Expected a class with a structDataclass decorator '
                         'to generate a standard dataclass __str__ method')
        self.assertEqual(QuaternionWithExtra(x=1, y=2, z=3, w=4).a, 0.1,
                         'Expected a class with a structDataclass decorator to provide '
                         'an accessor to pure dataclass fields')
        self.assertEqual(str(QuaternionWithExtra({'x': 1, 'y': 2, 'z': 3, 'w': 4}))[-54:],
                         'QuaternionWithExtra(x=1.0, y=2.0, z=3.0, w=4.0, a=0.1)',
                         'Expected a class with a structDataclass decorator '
                         'to generate a standard dataclass __str__ method')
        self.assertEqual(QuaternionWithExtra({'x': 1, 'y': 2, 'z': 3, 'w': 4}).a, 0.1,
                         'Expected a class with a structDataclass decorator to provide '
                         'an accessor to pure dataclass fields')
        self.assertEqual(QuaternionWithExtra(x=1, y=2, z=3, w=4, a=0.2).a, 0.2,
                         'Expected the constructor of a structDataclass to allow '
                         'setting an optional pure dataclass field')
        self.assertEqual(QuaternionWithExtra({'x': 1, 'y': 2, 'z': 3, 'w': 4}, a=0.2).a, 0.2,
                         'Expected the constructor of a structDataclass to allow '
                         'setting an optional pure dataclass field')

    def testEquality(self):
        """ Test that the data types can be compared for equality. """
        self.assertEqual(EnumType('Test', [('A', 1), ('B', 2)]), EnumType('Test', [('A', 1), ('B', 2)]),
                         msg='Expected two EnumTypes with the same name and members to be equal')
        self.assertEqual(EnumType('Test', [('A', 1), ('B', 2)]), EnumType('Test2', [('A', 1), ('B', 2)]),
                         msg='Expected two EnumTypes with the different names to be equal')
        self.assertNotEqual(EnumType('Test', [('A', 1), ('B', 2)]), EnumType('Test', [('C', 1), ('B', 2)]),
                            msg='Expected two EnumTypes with different members not to be equal')
        self.assertNotEqual(EnumType('Test', [('A', 1), ('B', 2)]), EnumType('Test', [('A', 0), ('B', 2)]),
                            msg='Expected two EnumTypes with different members not to be equal')

        def createTestArrayType(elementType, size: Optional[Union[int, str]] = None, name: str = 'Test'):
            args = {'__type__': elementType}
            if size is not None:
                args['__size__'] = size
            return type(name, (ArrayType,), args)

        uint8TypeInfo = TypeInfo.lookupBaseType(TypeInfo.BaseType.UINT8)
        floatTypeInfo = TypeInfo.lookupBaseType(TypeInfo.BaseType.FLOAT)

        self.assertEqual(createTestArrayType(uint8TypeInfo), createTestArrayType(uint8TypeInfo),
                         msg='Expected two ArrayTypes with the same element type and size to be equal')
        self.assertEqual(createTestArrayType(uint8TypeInfo, size=1), createTestArrayType(uint8TypeInfo, size=1),
                         msg='Expected two ArrayTypes with the same element type and size to be equal')
        self.assertEqual(createTestArrayType(uint8TypeInfo, size='member'),
                         createTestArrayType(uint8TypeInfo, size='member'),
                         msg='Expected two ArrayTypes with the same element type and size to be equal')
        self.assertEqual(createTestArrayType(uint8TypeInfo, name='Test1'),
                         createTestArrayType(uint8TypeInfo, name='Test2'),
                         msg='Expected two ArrayTypes with the same element type and size to be equal')
        self.assertNotEqual(createTestArrayType(uint8TypeInfo), createTestArrayType(floatTypeInfo),
                            msg='Expected two ArrayTypes with different element types not to be equal')
        self.assertNotEqual(createTestArrayType(uint8TypeInfo, size=1), createTestArrayType(uint8TypeInfo, size=2),
                            msg='Expected two ArrayTypes with different sizes not to be equal')
        self.assertNotEqual(createTestArrayType(uint8TypeInfo, size='member'),
                            createTestArrayType(uint8TypeInfo, size='member2'),
                            msg='Expected two ArrayTypes with different size sources not to be equal')

        def createTestStructType(childrenTypes, name: str = 'Test'):
            return type(name, (StructType,), {'__children__': childrenTypes})

        self.assertEqual(createTestStructType({'a': uint8TypeInfo, 'b': floatTypeInfo}),
                         createTestStructType({'a': uint8TypeInfo, 'b': floatTypeInfo}),
                         msg='Expected two StructTypes with the same element types to be equal')
        self.assertEqual(createTestStructType({'a': uint8TypeInfo, 'b': floatTypeInfo}, name='Test1'),
                         createTestStructType({'a': uint8TypeInfo, 'b': floatTypeInfo}, name='Test2'),
                         msg='Expected two StructTypes with the same element types to be equal')
        self.assertNotEqual(createTestStructType({'a': uint8TypeInfo, 'b': floatTypeInfo}),
                            createTestStructType({'a': uint8TypeInfo, 'c': floatTypeInfo}),
                            msg='Expected two StructTypes with different element types not to be equal')
        self.assertNotEqual(createTestStructType({'a': uint8TypeInfo, 'b': floatTypeInfo}),
                            createTestStructType({'a': uint8TypeInfo, 'b': uint8TypeInfo}),
                            msg='Expected two StructTypes with different element types not to be equal')

    def testFormats(self):
        """ Test that format strings are generated correctly. """
        database = CommunicationDatabase(TEST_DATA_DIR)
        arrayTypeInfo = database.parseKnownTypeInfo('Quaternion[8]')
        elementTypeInfo = database.parseKnownTypeInfo('Quaternion')
        self.assertEqual(arrayTypeInfo.getFormats(database), [elementTypeInfo.getFormats(database)[0] * 8])
        dynamicArrayTypeInfo = database.parseKnownTypeInfo('Quaternion[.size]')
        self.assertEqual(dynamicArrayTypeInfo.getFormats(database), ['', elementTypeInfo.getFormats(database)[0]])
